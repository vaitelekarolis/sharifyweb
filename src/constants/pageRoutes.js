// eslint-disable-next-line import/prefer-default-export
export const pageRoutes = {
  HOME: '/home',
  LOGIN: '/login',
  SIGN_UP: '/signUp',
  LIKED_SONGS: '/likedSongs',
  SONGS: '/songs',
  PLAYLISTS: '/playlists',
  MY_PLAYLISTS: '/myPlaylists',
  LIKED_PLAYLISTS: '/likedPlaylists',
  SONG_REQUESTS: '/songRequests',
  USERS: '/users',
  PROFILE: '/profile',
};
