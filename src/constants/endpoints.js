/* eslint-disable import/prefer-default-export */
export const endpoints = {
  AUTHENTICATE: 'User/Authenticate',
  REGISTER: 'User/Register',
  LOGOUT: 'User/Logout',

  SONG: 'Song',
  SONG_TOP_PLAYED: 'Song/TopPlayed',
  SONG_NEWEST: 'Song/Newest',
  SONG_LIKED: 'Song/Liked',
  SONG_CREATED_BY: 'Song/CreatedBy',
  SONG_MY: 'Song/My',
  SONG_GENRES: 'Song/Genres',
  SONG_STATUSES: 'Song/Statuses',
  SONG_REQUESTS: 'Song/Requests',
  SONG_CONFIRM: 'Song/Confirm',
  SONG_REJECT: 'Song/Reject',
  SONG_PLAY: 'Song/Play',
  TOGGLE_LIKE_SONG: 'Song/ToggleLike',

  PLAYLIST: 'Playlist',
  SHARED_PLAYLISTS: 'Playlist/Shared',
  MY_PLAYLISTS: 'Playlist/My',
  LIKED_PLAYLISTS: 'Playlist/Liked',
  SONGS_PLAYLISTS: 'Playlist/SongsPlaylists',
  TOGGLE_LIKE_PLAYLIST: 'Playlist/ToggleLike',
  TOGGLE_SHARE_PLAYLIST: 'Playlist/ToggleShare',
  PLAYLIST_SONG: 'Playlist/Song',

  USER: 'User',
  ALL_USERS: 'User/All',
  USER_PROFILE_PICTURE: 'User/ProfilePicture',
  USER_DESCRIPTION: 'User/Description',
};
