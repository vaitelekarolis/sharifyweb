export default {
  CONFIRMED: 'Confirmed',
  PENDING: 'Pending',
  REJECTED: 'Rejected',
};
