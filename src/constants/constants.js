import { pageRoutes } from './pageRoutes';

export const ROLE_KEY = 'role';
export const FIRST_NAME_KEY = 'firstName';
export const LAST_NAME_KEY = 'lastName';
export const EMAIL_KEY = 'email';

export const songsStorageUrl = 'gs://sharifystorage.appspot.com';

export const pageNamesByRoute = {
  [pageRoutes.LOGIN]: 'Login',
  [pageRoutes.SIGN_UP]: 'Sign up',
  [pageRoutes.LIKED_SONGS]: 'Liked songs',
  [pageRoutes.SONGS]: 'Songs',
  [pageRoutes.PLAYLISTS]: 'Playlists',
  [pageRoutes.MY_PLAYLISTS]: 'My playlists',
  [pageRoutes.LIKED_PLAYLISTS]: 'Liked playlists',
  [pageRoutes.SONG_REQUESTS]: 'Song uploads',
  [pageRoutes.USERS]: 'Users',
  [pageRoutes.PROFILE]: 'Profile',
  [pageRoutes.HOME]: '',
};
