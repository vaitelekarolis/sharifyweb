import axios from 'axios';
import localStorageService from './localStorage';
import toastService from './toast';
import urlService from './url';

const methods = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete',
};

async function makeRequest(method, path, data = null, params = null) {
  try {
    const url = urlService.getApiUrl(path);
    const response = await axios({
      method,
      url,
      data,
      withCredentials: true,
      params,
      // paramsSerializer(p) {
      //   return QueryString.stringify(p, { arrayFormat: 'repeat' });
      // },
    });
    return response.data || response.status;
  } catch (error) {
    if (error.response?.data?.Message) {
      toastService.error(error.response.data.Message);
    } else if (error.response.status === 400) {
      toastService.error('Validation error');
    } else if (error.response.status === 401) {
      localStorageService.logout();
    } else {
      toastService.error('Can\'t connect to server');
    }
  }
  return null;
}

async function get(path, params = null) {
  return makeRequest(methods.GET, path, null, params);
}

async function post(path, data) {
  return makeRequest(methods.POST, path, data);
}

async function put(path, data) {
  return makeRequest(methods.PUT, path, data);
}

async function remove(path, data = null) {
  return makeRequest(methods.DELETE, path, data);
}

const httpClientService = {
  get,
  post,
  put,
  remove,
};

export default httpClientService;
