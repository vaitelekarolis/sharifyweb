const PLAYED_SONGS_KEY = 'playedSongs';
const NEXT_SONG_KEY = 'nextSong';
const PREVIOUS_SONG_KEY = 'previousSong';
const PLAYLIST_KEY = 'playlist';
const SHUFFLE_KEY = 'shuffle';

function setPlayedSongs(songs) {
  localStorage[PLAYED_SONGS_KEY] = JSON.stringify(songs);
}

function getPlayedSongs() {
  return localStorage[PLAYED_SONGS_KEY]
    ? JSON.parse(localStorage[PLAYED_SONGS_KEY])
    : [];
}

function getLastSongId() {
  const currentPlayedSongs = getPlayedSongs();
  return currentPlayedSongs.length ? currentPlayedSongs[currentPlayedSongs.length - 1] : null;
}

function setNextSongId(songId) {
  if (songId) {
    localStorage[NEXT_SONG_KEY] = songId;
  } else {
    localStorage.removeItem(NEXT_SONG_KEY);
  }
}

function getNextSongId() {
  return localStorage[NEXT_SONG_KEY];
}

function setPreviousSongId(songId) {
  if (songId) {
    localStorage[PREVIOUS_SONG_KEY] = songId;
  } else {
    localStorage.removeItem(PREVIOUS_SONG_KEY);
  }
}

function getPreviousSongId() {
  return localStorage[PREVIOUS_SONG_KEY];
}

function setPlaylistId(playlistId) {
  if (playlistId) {
    localStorage[PLAYLIST_KEY] = playlistId;
  } else {
    localStorage.removeItem(PLAYLIST_KEY);
  }
}

function getPlaylistId() {
  return localStorage[PLAYLIST_KEY];
}

function setShuffle(shuffle) {
  if (shuffle) {
    localStorage[SHUFFLE_KEY] = shuffle;
  } else {
    localStorage.removeItem(SHUFFLE_KEY);
  }
}

function getShuffle() {
  return Boolean(localStorage[SHUFFLE_KEY]);
}

function clearStorage() {
  localStorage.removeItem(PLAYED_SONGS_KEY);
  localStorage.removeItem(NEXT_SONG_KEY);
  localStorage.removeItem(PREVIOUS_SONG_KEY);
  localStorage.removeItem(SHUFFLE_KEY);
  localStorage.removeItem(PLAYLIST_KEY);
}

const songStorageService = {
  setPlayedSongs,
  getPlayedSongs,
  getLastSongId,
  setNextSongId,
  getNextSongId,
  setPreviousSongId,
  getPreviousSongId,
  setPlaylistId,
  getPlaylistId,
  setShuffle,
  getShuffle,
  clearStorage,
};

export default songStorageService;
