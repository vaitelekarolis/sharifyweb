import { getDownloadURL, getStorage, ref } from '@firebase/storage';
import { endpoints } from '../constants/endpoints';
import httpClientService from './httpClient';

async function getAllAsync(filter) {
  const result = await httpClientService.get(endpoints.SONG, filter);
  return result?.songs || [];
}

async function getTopPlayedAsync() {
  const result = await httpClientService.get(endpoints.SONG_TOP_PLAYED);
  return result?.songs || [];
}

async function getNewestAsync() {
  const result = await httpClientService.get(endpoints.SONG_NEWEST);
  return result?.songs || [];
}

async function getByIdAsync(id) {
  return httpClientService.get(`${endpoints.SONG}/${id}`);
}

async function getLikedAsync(filter) {
  const result = await httpClientService.get(endpoints.SONG_LIKED, filter);
  return result?.songs || [];
}

async function getCreatedByAsync(filter, userName, takeTop = null) {
  const result = await httpClientService.get(`${endpoints.SONG_CREATED_BY}/${userName}`, { takeTop, ...filter });
  return result?.songs || [];
}

async function getMyAsync(filter) {
  const result = await httpClientService.get(endpoints.SONG_MY, filter);
  return result?.songs || [];
}

async function getGenresAsync() {
  return httpClientService.get(endpoints.SONG_GENRES);
}

async function getStatusesAsync() {
  return httpClientService.get(endpoints.SONG_STATUSES);
}

async function getSongRequestsAsync(filter) {
  const result = await httpClientService.get(endpoints.SONG_REQUESTS, filter);
  return result?.songs || [];
}

async function getPlayAsync(data) {
  return httpClientService.put(endpoints.SONG_PLAY, data);
}

async function createAsync(song) {
  return httpClientService.post(endpoints.SONG, song);
}

async function confirmSongAsync(id) {
  return httpClientService.put(endpoints.SONG_CONFIRM, { id });
}

async function rejectSongAsync(id) {
  return httpClientService.put(endpoints.SONG_REJECT, { id });
}

async function getSongBlobUrl(songName) {
  const storage = getStorage();
  return getDownloadURL(ref(storage, songName))
    .then((url) => url);
}

async function toggleLikeAsync(id) {
  return httpClientService.put(endpoints.TOGGLE_LIKE_SONG, { id });
}

async function deleteSongAsync(id) {
  return httpClientService.remove(endpoints.SONG, { id });
}

const songService = {
  getAllAsync,
  getTopPlayedAsync,
  getNewestAsync,
  getByIdAsync,
  getLikedAsync,
  getCreatedByAsync,
  getMyAsync,
  getGenresAsync,
  getStatusesAsync,
  getSongRequestsAsync,
  getPlayAsync,
  createAsync,
  confirmSongAsync,
  rejectSongAsync,
  getSongBlobUrl,
  toggleLikeAsync,
  deleteSongAsync,
};

export default songService;
