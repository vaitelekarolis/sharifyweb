function getApiUrl(path) {
  return process.env.REACT_APP_API_URL + path;
}

const urlService = {
  getApiUrl,
};

export default urlService;
