import songStorageService from './songStorage';

const IS_LOGGED_IN_KEY = 'isLoggedIn';

function login() {
  localStorage.setItem(IS_LOGGED_IN_KEY, true);
}

function logout() {
  localStorage.removeItem(IS_LOGGED_IN_KEY);
  songStorageService.clearStorage();
}

function isLoggedIn() {
  return localStorage.getItem(IS_LOGGED_IN_KEY);
}

const localStorageService = {
  login,
  logout,
  isLoggedIn,
};

export default localStorageService;
