import { endpoints } from '../constants/endpoints';
import httpClientService from './httpClient';
import localStorageService from './localStorage';

async function getInfoAsync() {
  const result = await httpClientService.get(endpoints.USER);
  return result;
}

async function getByNameAsync(username) {
  const result = await httpClientService.get(`${endpoints.USER}/${username}`);
  return result;
}

async function authenticateAsync(email, password) {
  const result = await httpClientService.post(endpoints.AUTHENTICATE,
    { email, password });
  if (result) {
    localStorageService.login();
  }
  return result;
}

async function logoutAsync() {
  const result = await httpClientService.post(endpoints.LOGOUT);
  localStorageService.logout();
  return result;
}

async function registerAsync(username, email, password) {
  const result = await httpClientService.post(endpoints.REGISTER,
    {
      username, email, password,
    });
  return result;
}

async function getAllAsync() {
  const result = await httpClientService.get(endpoints.ALL_USERS);
  return result;
}

async function updateProfilePictureAsync(profilePicture) {
  return httpClientService.put(endpoints.USER_PROFILE_PICTURE, { profilePicture });
}

async function updateDescriptionAsync(description) {
  return httpClientService.put(endpoints.USER_DESCRIPTION, { description });
}

async function deleteAccountAsync() {
  return httpClientService.remove(endpoints.USER);
}

const userService = {
  authenticateAsync,
  logoutAsync,
  registerAsync,
  getInfoAsync,
  getByNameAsync,
  getAllAsync,
  updateProfilePictureAsync,
  updateDescriptionAsync,
  deleteAccountAsync,
};

export default userService;
