import { toast } from 'react-toastify';

function error(message) {
  toast(message, { type: 'error' });
}

function success(message) {
  toast(message, { type: 'success' });
}

function info(message) {
  toast(message, { type: 'info' });
}

const toastService = {
  error,
  success,
  info,
};

export default toastService;
