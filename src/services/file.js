function convertToBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result?.split(',')[1]);
    reader.onerror = (error) => reject(error);
  });
}

function base64toBlob(base64File) {
  const buffer = Buffer.from(base64File, 'base64');
  const blob = new Blob([buffer]);

  return blob;
}

const fileService = {
  convertToBase64,
  base64toBlob
};

export default fileService;
