function isSongReferenceValidUrl(reference) {
  try {
    const url = new URL(reference);
    return url.protocol === 'http:' || url.protocol === 'https:';
  } catch (_) {
    return false;
  }
}

const validationService = {
  isSongReferenceValidUrl,
};

export default validationService;
