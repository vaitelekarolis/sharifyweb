import { endpoints } from '../constants/endpoints';
import httpClientService from './httpClient';

async function getSharedAsync(filter) {
  const result = await httpClientService.get(endpoints.SHARED_PLAYLISTS, filter);
  return result?.playlists || [];
}

async function getMyAsync(filter) {
  const result = await httpClientService.get(endpoints.MY_PLAYLISTS, filter);
  return result?.playlists || [];
}

async function getLikedAsync(filter) {
  const result = await httpClientService.get(endpoints.LIKED_PLAYLISTS, filter);
  return result?.playlists || [];
}

async function getSongsPlaylistsAsync(songId) {
  return httpClientService.get(`${endpoints.SONGS_PLAYLISTS}/${songId}`);
}

async function getByIdAsync(id) {
  return httpClientService.get(`${endpoints.PLAYLIST}/${id}`);
}

async function getSongsAsync(filter, id) {
  const result = await httpClientService.get(`${endpoints.PLAYLIST}/${id}/Songs`, filter);
  return result?.songs || [];
}

async function createAsync(playlist) {
  return httpClientService.post(endpoints.PLAYLIST, playlist);
}

async function addSongAsync(playlistId, songId) {
  return httpClientService.post(endpoints.PLAYLIST_SONG, { playlistId, songId });
}

async function removeSongAsync(playlistId, songId) {
  return httpClientService.remove(endpoints.PLAYLIST_SONG, { playlistId, songId });
}

async function toggleLikeAsync(id) {
  return httpClientService.put(endpoints.TOGGLE_LIKE_PLAYLIST, { id });
}

async function toggleShareAsync(id) {
  return httpClientService.put(endpoints.TOGGLE_SHARE_PLAYLIST, { id });
}

async function updatePlaylistAsync(id, description) {
  return httpClientService.put(endpoints.PLAYLIST, { id, description });
}

async function deletePlaylistAsync(id) {
  return httpClientService.remove(endpoints.PLAYLIST, { id });
}

const playlistService = {
  getSharedAsync,
  getMyAsync,
  getSongsPlaylistsAsync,
  getByIdAsync,
  getSongsAsync,
  getLikedAsync,
  createAsync,
  addSongAsync,
  removeSongAsync,
  toggleLikeAsync,
  toggleShareAsync,
  updatePlaylistAsync,
  deletePlaylistAsync,
};

export default playlistService;
