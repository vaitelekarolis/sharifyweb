const base64Signature = /^data:image\/(png|jpg);base64,/;

function resize(img, maxWidth, maxHeight) {
  let { width, height } = img;

  if (width > height && width > maxWidth) {
    height *= maxWidth / width;
    width = maxWidth;
  } else if (height > maxHeight) {
    width *= maxHeight / height;
    height = maxHeight;
  }

  return { width, height };
}

function getBase64fromUrl(url, maxWidth = 100, maxHeight = 100) {
  if (!url) return null;

  return new Promise((resolve) => {
    const img = new Image();

    img.onload = () => {
      const canvas = document.createElement('canvas');
      const resizedDimensions = resize(img, maxWidth, maxHeight);
      canvas.width = resizedDimensions.width;
      canvas.height = resizedDimensions.height;

      const context = canvas.getContext('2d');
      context.drawImage(img, 0, 0, canvas.width, canvas.height);

      const imgData = canvas.toDataURL('image/png');
      const base64ImgData = imgData.replace(base64Signature, '');

      resolve(base64ImgData);
    };

    img.src = url;
  });
}

function getImageFromBase64(base64) {
  if (!base64) return '';
  return `data:image/png;base64,${base64}`;
}

const imageService = {
  getBase64fromUrl,
  getImageFromBase64,
};

export default imageService;
