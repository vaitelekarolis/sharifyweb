function formatSongTime(seconds) {
  if (!seconds && seconds !== 0) return null;
  return new Date(Math.floor(seconds) * 1000).toISOString().substring(14, 19);
}

const formatService = {
  formatSongTime,
};

export default formatService;
