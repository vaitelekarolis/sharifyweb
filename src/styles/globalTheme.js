import { createTheme } from '@mui/material';

const font = "'Noto Sans', sans-serif";

export const mainColor = '#FF8D5A';
export const lightColor = '#FFA279';
export const darkColor = '#D06332';

export const globalTheme = createTheme({
  palette: {
    primary: {
      main: mainColor,
      light: lightColor,
      dark: darkColor,
    },
  },
  typography: {
    fontFamily: font,
  },
});

export default globalTheme;
