import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddBoxIcon from '@mui/icons-material/AddBox';
import CloseIcon from '@mui/icons-material/Close';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import HomeIcon from '@mui/icons-material/Home';
import LibraryMusicIcon from '@mui/icons-material/LibraryMusic';
import MusicNoteIcon from '@mui/icons-material/MusicNote';
import MusicVideoIcon from '@mui/icons-material/MusicVideo';
import {
  Divider
} from '@mui/material';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import logo from '../../public/logo.png';
import localStorageService from '../../services/localStorage';
import styles from '../../styles/layout/Sidebar.module.scss';
import { UserGeneralInfo } from '../ContextProvider';
import LinkWithIcon from '../LinkWithIcon';

export default function Sidebar({ isMobileMenuOpen, setIsMobileMenuOpen, isMobile }) {
  const { userGeneralInfo } = useContext(UserGeneralInfo);

  const renderMainLinks = () => (
    <>
      <LinkWithIcon route={pageRoutes.HOME}>
        <HomeIcon />
        Home
      </LinkWithIcon>
      <LinkWithIcon
        route={pageRoutes.SONGS}
      >
        <MusicNoteIcon />
        Songs
      </LinkWithIcon>
      {localStorageService.isLoggedIn() && userGeneralInfo.role
        && (
          <>
            <LinkWithIcon
              route={pageRoutes.LIKED_SONGS}
            >
              <FavoriteBorderIcon />
              Liked Songs
            </LinkWithIcon>
            <LinkWithIcon
              route={pageRoutes.PLAYLISTS}
            >
              <LibraryMusicIcon />
              Playlists
            </LinkWithIcon>
            <LinkWithIcon
              route={pageRoutes.LIKED_PLAYLISTS}
            >
              <FavoriteIcon />
              Liked Playlists
            </LinkWithIcon>
            <LinkWithIcon
              route={pageRoutes.MY_PLAYLISTS}
            >
              <MusicVideoIcon />
              My Playlists
            </LinkWithIcon>
            <LinkWithIcon route={`${pageRoutes.PROFILE}/${userGeneralInfo.name}`}>
              <AccountCircleIcon />
              Profile
            </LinkWithIcon>
          </>
        )}
      {localStorageService.isLoggedIn() && userGeneralInfo.role === 'Admin' && (
      <>
        <Divider style={{ background: 'rgb(200, 200, 200)' }} />
        <LinkWithIcon
          route={pageRoutes.SONG_REQUESTS}
        >
          <AddBoxIcon />
          Song Uploads
        </LinkWithIcon>
        {/* <LinkWithIcon
          route={pageRoutes.USERS}
        >
          <SupervisedUserCircleIcon />
          Users
        </LinkWithIcon> */}
      </>
      )}
    </>
  );

  return (
    <div className={`${styles.Sidebar} ${(isMobile && isMobileMenuOpen) ? styles.MobileSidebar : ''}`}>
      {isMobile && (
      <CloseIcon
        className={styles.CloseIcon}
        onClick={() => setIsMobileMenuOpen(false)}
      />
      )}
      <Link
        className={styles.Logo}
        to={pageRoutes.HOME}
      >
        <img
          src={logo}
          alt="Logo"
        />
      </Link>
      <Divider style={{ background: 'rgb(200, 200, 200)' }} />
      {renderMainLinks()}
    </div>
  );
}

Sidebar.propTypes = {
  isMobileMenuOpen: PropTypes.bool.isRequired,
  setIsMobileMenuOpen: PropTypes.func.isRequired,
  isMobile: PropTypes.bool.isRequired,
};
