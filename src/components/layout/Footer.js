/* eslint-disable max-lines */
/* eslint-disable jsx-a11y/media-has-caption */
import { QueueMusic, Shuffle } from '@mui/icons-material';
import PauseIcon from '@mui/icons-material/Pause';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import VolumeOffIcon from '@mui/icons-material/VolumeOff';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import {
  useContext, useEffect, useRef, useState
} from 'react';
import { useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import formatService from '../../services/format';
import songService from '../../services/song';
import toastService from '../../services/toast';
import styles from '../../styles/layout/Footer.module.scss';
import { Player } from '../ContextProvider';
import SongTitle from '../song/SongTitle';

export default function Footer() {
  const {
    playedSongs,
    setNextSongId,
    setPreviousSongId,
    shuffle,
    changeShuffle,
    playlistId,
    currentSongId,
    playNext,
    playPrevious,
    canPlayNext,
    canPlayPrevious,
    autoPlay,
    setAutoPlay,
    isPlaying,
    setIsPlaying,
  } = useContext(Player);
  const history = useHistory();
  const [currentSong, setCurrentSong] = useState({});
  const [songUrl, setSongUrl] = useState('');
  const [currentTime, setCurrentTime] = useState(0);
  const [volume, setVolume] = useState(70);
  const [progressValue, setProgressValue] = useState(0);
  const audioElement = useRef();

  const isPlayerDisabled = !currentSong.id;

  const onProgressBarChange = (e) => {
    if (audioElement.current) {
      setProgressValue(e.target.value);
      audioElement.current.currentTime = (e.target.value
        * audioElement.current.duration) / 1000;
      setCurrentTime(audioElement.current.currentTime);
    }
  };

  const onVolumeBarChange = (e) => {
    if (audioElement.current) {
      audioElement.current.volume = e.target.value / 100;
      setVolume(e.target.value);
    }
  };

  const onEnded = () => {
    if (canPlayNext()) {
      setSongUrl('');
      playNext();
      setAutoPlay(true);
    } else {
      setIsPlaying(false);
    }
  };

  const onCanPlay = () => {
    if (autoPlay) {
      setIsPlaying(true);
      setAutoPlay(false);
    }
  };

  useEffect(() => {
    setProgressValue(0);
    if (currentSongId) {
      songService.getPlayAsync({
        songId: currentSongId, playlistId, playedSongs, shuffle
      })
        .then((result) => {
          if (result) {
            setCurrentSong(result);
            setNextSongId(result.nextSongId);
            setPreviousSongId(result.previousSongId);
          }
        })
        .catch((error) => toastService.error(error));
    } else {
      setCurrentSong({});
      setCurrentTime(0);
    }
  }, [currentSongId, shuffle]);

  const updateProgressBar = () => {
    if (audioElement.current) {
      setProgressValue((audioElement.current.currentTime
        / audioElement.current.duration) * 1000);
      setCurrentTime(audioElement.current.currentTime);
    }
  };

  useEffect(() => {
    if (currentSong.songName) {
      songService.getSongBlobUrl(currentSong.songName)
        .then((result) => {
          setSongUrl(result);
          setCurrentTime(0);
        });
    } else {
      setSongUrl('');
    }
  }, [currentSong]);

  useEffect(() => {
    if (!audioElement.current) return;
    if (isPlaying) {
      audioElement.current.play();
    } else {
      audioElement.current.pause();
    }
  }, [isPlaying, songUrl]);

  return (
    <footer className={styles.Footer}>
      <audio
        src={songUrl}
        ref={audioElement}
        onPlaying={() => !isPlaying || setIsPlaying(true)}
        onPause={() => isPlaying || setIsPlaying(false)}
        onEnded={onEnded}
        onCanPlayThrough={onCanPlay}
        onTimeUpdate={updateProgressBar}
        hidden
      />
      <div className={styles.Description}>
        {currentSong.title && <SongTitle song={currentSong} isFooter />}
      </div>
      <div className={styles.Player}>
        <div className={styles.Controls}>
          <button
            type="button"
            className={styles.OtherControl}
            disabled={!playlistId}
            onClick={() => history.push(`${pageRoutes.PLAYLISTS}/${playlistId}`)}
          >
            <QueueMusic className={styles.OtherControlIcon} />
          </button>
          <button
            type="button"
            className={styles.Skip}
            disabled={isPlayerDisabled || !canPlayPrevious()}
            onClick={playPrevious}
          >
            <SkipPreviousIcon />
          </button>
          <button
            type="button"
            className={styles.Play}
            disabled={isPlayerDisabled}
            onClick={() => setIsPlaying(!isPlaying)}
          >
            {isPlaying
              ? <PauseIcon />
              : <PlayArrowIcon />}
          </button>
          <button
            type="button"
            className={styles.Skip}
            disabled={isPlayerDisabled || !canPlayNext()}
            onClick={playNext}
          >
            <SkipNextIcon />
          </button>
          <button
            type="button"
            className={styles.OtherControl}
            disabled={!playlistId}
            onClick={changeShuffle}
          >
            <Shuffle
              className={shuffle && Boolean(playlistId)
                ? styles.SelectedControlIcon
                : styles.OtherControlIcon}
            />
          </button>
        </div>
        <div className={styles.Slider}>
          {songUrl && formatService.formatSongTime(currentTime)}
          <input
            className={styles.InputSlider}
            style={{ background: `linear-gradient(to right, #ffaf37 ${progressValue / 10}%, rgb(145, 145, 145) ${progressValue / 10}% 100%)` }}
            type="range"
            defaultValue={0}
            min={0}
            max={1000}
            value={progressValue || 0}
            onMouseDown={() => setIsPlaying(false)}
            onChange={onProgressBarChange}
            onMouseUp={() => setIsPlaying(true)}
            disabled={isPlayerDisabled}
          />
          {formatService.formatSongTime(audioElement.current?.duration)}
        </div>
      </div>
      <div className={styles.Actions}>
        {Number(volume) ? <VolumeUpIcon /> : <VolumeOffIcon />}
        <input
          type="range"
          style={{ background: `linear-gradient(to right, rgb(230, 230, 230) ${volume}%, rgb(100, 100, 100) ${volume}% 100%)` }}
          onChange={onVolumeBarChange}
          min={0}
          max={100}
          value={volume}
          className={styles.VolumeSlider}
        />
      </div>
    </footer>
  );
}
