/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { MenuOpen } from '@mui/icons-material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import { Button, Menu, MenuItem } from '@mui/material';
import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useLocation } from 'react-router-dom/cjs/react-router-dom.min';
import { pageNamesByRoute } from '../../constants/constants';
import { pageRoutes } from '../../constants/pageRoutes';
import imageService from '../../services/image';
import localStorageService from '../../services/localStorage';
import userService from '../../services/user';
import styles from '../../styles/layout/Header.module.scss';
import { Player, UserGeneralInfo } from '../ContextProvider';
import CreatePlaylistModal from '../playlist/CreatePlaylistModal';
import CreateSongModal from '../song/CreateSongModal';

export default function Header({ isMobileMenuOpen, setIsMobileMenuOpen, isMobile }) {
  const history = useHistory();
  const location = useLocation();

  const { userGeneralInfo, updateUserGeneralInfoAsync } = useContext(UserGeneralInfo);
  const { clearPlayer } = useContext(Player);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isSongModalOpen, setIsSongModalOpen] = useState(false);
  const [isPlaylistModalOpen, setIsPlaylistModalOpen] = useState(false);
  const open = Boolean(anchorEl);

  const getPageName = () => {
    const key = Object.keys(pageNamesByRoute).find((name) => location.pathname.startsWith(name));
    return pageNamesByRoute[key];
  };

  const openProfile = () => {
    history.push(`${pageRoutes.PROFILE}/${userGeneralInfo.name}`);
    setAnchorEl(null);
  };

  const openSongModal = () => {
    setIsSongModalOpen(true);
    setAnchorEl(null);
  };
  const openPlaylistModal = () => {
    setIsPlaylistModalOpen(true);
    setAnchorEl(null);
  };

  const logoutAsync = async () => {
    await userService.logoutAsync();
    await updateUserGeneralInfoAsync();
    clearPlayer();
    setAnchorEl(null);
    history.push(pageRoutes.HOME);
  };

  const menuOptions = [
    {
      text: 'Profile',
      onClick: openProfile,
    },
    {
      text: 'Upload song',
      onClick: openSongModal,
    },
    {
      text: 'Create playlist',
      onClick: openPlaylistModal,
    },
    {
      text: 'Logout',
      onClick: logoutAsync,
    },
  ];

  const renderAccountMenuItems = () => (
    menuOptions.map((option) => (
      <MenuItem key={option.text} onClick={option.onClick}>
        {option.text}
      </MenuItem>
    ))
  );

  return (
    <header>
      <div className={styles.Header}>
        <div className={styles.Route}>
          <HomeIcon className={styles.HomeIcon} onClick={() => history.push(pageRoutes.HOME)} />
          {` / ${getPageName()}`}
        </div>
        <div className={styles.Navigation}>
          {isMobile && (isMobileMenuOpen
            ? (
              <MenuOpen
                onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
                className={styles.Icon}
              />
            )
            : (
              <MenuIcon
                onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
                className={styles.Icon}
              />
            ))}
          {localStorageService.isLoggedIn() ? (
            <>
              <Button onClick={(e) => setAnchorEl(e.currentTarget)} className={styles.UserButton}>
                <div>
                  {userGeneralInfo.name}
                </div>
                {userGeneralInfo.picture
                  ? (
                    <img
                      src={imageService.getImageFromBase64(userGeneralInfo.picture)}
                      className={styles.ProfilePicture}
                      alt="Profile"
                    />
                  )
                  : (
                    <AccountCircleIcon
                      className={styles.AccountIcon}
                    />
                  )}
              </Button>
              <Menu
                id="account-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={() => setAnchorEl(null)}
                PaperProps={{
                  style: {
                    maxHeight: 200,
                  },
                }}
              >
                {renderAccountMenuItems()}
              </Menu>
            </>
          ) : (
            <>
              <Button onClick={() => history.push(pageRoutes.SIGN_UP)}>
                Sign up
              </Button>
              <Button onClick={() => history.push(pageRoutes.LOGIN)}>
                Log in
              </Button>
            </>
          )}
        </div>
      </div>
      {isSongModalOpen
      && (
      <CreateSongModal
        isOpen={isSongModalOpen}
        onClose={() => setIsSongModalOpen(false)}
      />
      )}
      {isPlaylistModalOpen
      && (
      <CreatePlaylistModal
        isOpen={isPlaylistModalOpen}
        onClose={() => setIsPlaylistModalOpen(false)}
      />
      )}
    </header>
  );
}

Header.propTypes = {
  isMobileMenuOpen: PropTypes.bool.isRequired,
  setIsMobileMenuOpen: PropTypes.func.isRequired,
  isMobile: PropTypes.bool.isRequired,
};
