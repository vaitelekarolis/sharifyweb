import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import styles from '../styles/BasicModal.module.scss';

export default function BasicModal({ isOpen, onClose, children }) {
  return (
    <ReactModal
      isOpen={isOpen}
      overlayClassName={styles.Overlay}
      className={styles.Content}
      onRequestClose={onClose}
      shouldCloseOnEsc
      shouldCloseOnOverlayClick
      ariaHideApp={false}
      shouldFocusAfterRender={false}
    >
      <div className={styles.Wrapper}>
        <button
          className={styles.CloseButton}
          type="button"
          onClick={onClose}
        >
          <CloseIcon className={styles.CloseIcon} />
        </button>
        {children}
      </div>
    </ReactModal>
  );
}

BasicModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};
