import PropTypes from 'prop-types';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import songService from '../../services/song';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import SongList from '../song/SongList';

export default function Songs({ username }) {
  const [songs, setSongs] = useState([]);
  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    songService.getCreatedByAsync, setSongs, username
  );

  return (
    <>
      <Filter setIsLoading={setIsLoading} />
      <InfiniteScroll
        dataLength={songs.length}
        loadMore={nextPage}
        hasMore={!isLoading}
        threshold={150}
      >
        <SongList songs={songs} setSongs={setSongs} title={`Songs created by ${username}`} />
      </InfiniteScroll>
    </>
  );
}

Songs.propTypes = {
  username: PropTypes.string.isRequired,
};
