/* eslint-disable jsx-a11y/anchor-is-valid */
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useContext, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import { email } from '../../constants/regex';
import toastService from '../../services/toast';
import userService from '../../services/user';
import { UserGeneralInfo } from '../ContextProvider';

export default function LoginForm() {
  const history = useHistory();
  const { updateUserGeneralInfoAsync } = useContext(UserGeneralInfo);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [dirty, setDirty] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!isEmailValid) {
      toastService.error('Invalid email format');
      return;
    }
    const data = new FormData(event.currentTarget);
    const result = await userService.authenticateAsync(data.get('email'), data.get('password'));
    if (result) {
      await updateUserGeneralInfoAsync();
      history.push(pageRoutes.HOME);
    }
  };

  const onEmailChange = (e) => {
    const { value } = e.target;
    if (email.test(value)) {
      setIsEmailValid(true);
    } else {
      setIsEmailValid(false);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <Box
          component="form"
          onSubmit={handleSubmit}
          sx={{ mt: 1 }}
        >
          <TextField
            error={dirty && !isEmailValid}
            onBlur={() => setDirty(true)}
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={onEmailChange}
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Login
          </Button>
          <Link to={pageRoutes.SIGN_UP} style={{ color: '#42424a', textDecoration: 'underline', textDecorationColor: '#42424a' }}>
            {'Don\'t have an account? Sign Up'}
          </Link>
        </Box>
      </Box>
    </Container>
  );
}
