import { Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import userService from '../../services/user';
import UserListItem from './UserListItem';

export default function UserList() {
  const [users, setUsers] = useState([]);

  useEffect(async () => {
    const result = await userService.getAllAsync();
    setUsers(result.users);
  }, []);

  const renderUsers = () => (
    users.map((user) => (
      <UserListItem user={user} />
    ))
  );

  return (
    <div className="PageWrapper">
      <div className="PageTitle">
        Users
      </div>
      <div className="PageTableWrapper">
        <Grid
          container
          className="GridTable"
        >
          <Grid
            item
            container
            className="GridTableHeader"
          >
            <Grid item xs={3}>
              Username
            </Grid>
            <Grid item xs={6} sm={5} md={4}>
              Email
            </Grid>
            <Grid item xs={1} md={2}>
              Role
            </Grid>
          </Grid>
          {renderUsers()}
        </Grid>
      </div>
    </div>
  );
}
