import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import {
  Box,
  Button, Checkbox, CircularProgress, FormControlLabel, Typography
} from '@mui/material';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import toastService from '../../services/toast';
import userService from '../../services/user';
import styles from '../../styles/user/DeleteAccountModal.module.scss';
import BasicModal from '../BasicModal';
import { Player, UserGeneralInfo } from '../ContextProvider';

export default function DeleteAccountModal({ isOpen, onClose, onDelete }) {
  const { userGeneralInfo, setUserGeneralInfo } = useContext(UserGeneralInfo);
  const { clearPlayer } = useContext(Player);
  const history = useHistory();
  const [isAgreed, setIsAgreed] = useState(false);
  const [timer, setTimer] = useState(5);
  const [isLoading, setIsLoading] = useState(false);

  const deleteAccountAsync = async () => {
    setIsLoading(true);
    const result = await userService.deleteAccountAsync();
    setIsLoading(false);

    if (result) {
      onDelete();
      onClose();
      clearPlayer();
      setUserGeneralInfo({});
      await userService.logoutAsync();
      history.push(pageRoutes.HOME);
      toastService.info(`Account of '${userGeneralInfo.name}' has been deleted`);
    }
  };

  useEffect(() => {
    if (timer > 0) {
      const timeout = setTimeout(() => {
        setTimer((prevTimer) => prevTimer - 1);
      }, 1000);

      return () => {
        clearTimeout(timeout);
      };
    }

    return undefined;
  }, [timer]);

  return (
    <BasicModal isOpen={isOpen} onClose={onClose}>
      <div className={styles.Wrapper}>
        <div className={styles.Text}>
          <ReportGmailerrorredIcon className={styles.ErrorIcon} />
          <br />
          Are you sure you want to delete your account?
          <br />
          Once deleted you cannot undo this action.
        </div>
        <FormControlLabel
          control={(
            <Checkbox
              checked={isAgreed}
              onChange={() => setIsAgreed(!isAgreed)}
              style={{ color: '#d11313' }}
            />
          )}
          label="I understand and I want to delete my account."
        />
        <div className={styles.Buttons}>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button
            className={styles.DeleteButton}
            onClick={deleteAccountAsync}
            disabled={!isAgreed || timer > 0 || isLoading}
          >
            {timer > 0 ? (
              <Box display="flex" justifyContent="center" alignItems="center">
                <CircularProgress style={{ width: '24px', height: '24px' }} />
                <Typography position="absolute">{timer}</Typography>
              </Box>
            ) : 'Delete'}
          </Button>
        </div>
      </div>
    </BasicModal>
  );
}

DeleteAccountModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
