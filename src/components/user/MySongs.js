import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import songService from '../../services/song';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import MySongList from '../song/MySongList';

export default function MySongs() {
  const [songs, setSongs] = useState([]);
  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    songService.getMyAsync, setSongs
  );

  return (
    <>
      <Filter setIsLoading={setIsLoading} hasStatus />
      <InfiniteScroll
        dataLength={songs.length}
        loadMore={nextPage}
        hasMore={!isLoading}
        threshold={100}
      >
        <MySongList songs={songs} title="Your songs" />
      </InfiniteScroll>
    </>
  );
}
