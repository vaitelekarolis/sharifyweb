import { Grid } from '@mui/material';
import PropTypes from 'prop-types';

export default function UserListItem({ user }) {
  return (
    <Grid
      item
      container
      className="Row"
    >
      <Grid item xs={3}>
        {user.name}
      </Grid>
      <Grid item xs={6} sm={5} md={4}>
        {user.email}
      </Grid>
      <Grid item xs={1} md={2}>
        {user.role}
      </Grid>
    </Grid>
  );
}

UserListItem.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
    name: PropTypes.string,
    role: PropTypes.string,
  }).isRequired,
};
