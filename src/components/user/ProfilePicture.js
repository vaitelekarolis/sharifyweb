import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import PropTypes from 'prop-types';
import {
  useContext, useEffect, useRef, useState
} from 'react';
import { useParams } from 'react-router-dom';
import imageService from '../../services/image';
import userService from '../../services/user';
import styles from '../../styles/user/ProfilePicture.module.scss';
import { UserGeneralInfo } from '../ContextProvider';

export default function ProfilePicture({ picture }) {
  const { username } = useParams();
  const [selectedUrl, setSelectedUrl] = useState(null);
  const { updateUserGeneralInfoAsync, userGeneralInfo } = useContext(UserGeneralInfo);
  const inputRef = useRef();

  const onSelectFile = async (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      return;
    }
    const file = e.target.files[0];
    const imageUrl = URL.createObjectURL(file);

    const imgBase64 = await imageService.getBase64fromUrl(imageUrl);
    const result = await userService.updateProfilePictureAsync(imgBase64);
    if (result) {
      await updateUserGeneralInfoAsync();
    }
  };

  useEffect(() => {
    setSelectedUrl(imageService.getImageFromBase64(picture));
  }, [picture]);

  return (
    <div className={styles.Wrapper}>
      <div className={styles.Picture}>
        {selectedUrl
          ? <img src={selectedUrl} className={styles.ProfilePicture} alt="Profile" />
          : <AccountCircleIcon className={styles.PlaceholderIcon} />}
      </div>
      {userGeneralInfo?.name === username
      && (
      <button type="button" className={styles.AddButton}>
        <input type="file" onChange={onSelectFile} hidden ref={inputRef} accept="image/*" />
        <AddAPhotoIcon className={styles.AddIcon} onClick={() => inputRef.current.click()} />
      </button>
      )}
    </div>
  );
}

ProfilePicture.propTypes = {
  picture: PropTypes.string,
};

ProfilePicture.defaultProps = {
  picture: null,
};
