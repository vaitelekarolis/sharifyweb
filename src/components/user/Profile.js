import { Button } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import userService from '../../services/user';
import styles from '../../styles/user/Profile.module.scss';
import { UserGeneralInfo } from '../ContextProvider';
import EditableText from '../EditableText';
import GenresLink from '../GenresLink';
import DeleteAccountModal from './DeleteAccountModal';
import MySongs from './MySongs';
import ProfilePicture from './ProfilePicture';
import Songs from './Songs';

export default function Profile() {
  const { username } = useParams();
  const { userGeneralInfo } = useContext(UserGeneralInfo);
  const [userInfo, setUserInfo] = useState({});
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const isMine = userInfo?.name && userGeneralInfo?.name === userInfo?.name;

  const updateUserInfo = async () => {
    if (username) {
      const result = await userService.getByNameAsync(username);
      if (result) {
        setUserInfo(result);
      }
    }
  };

  const updateData = async () => {
    await updateUserInfo();
  };

  const onDescriptionChange = async (description) => {
    await userService.updateDescriptionAsync(description);
    await updateUserInfo();
  };

  useEffect(async () => {
    await updateData();
  }, [username]);

  return (
    <div className="PageWrapper">
      <div className={styles.Wrapper}>
        <div className={styles.MainWrapper}>
          <div className={styles.Header}>
            <div className={styles.User}>
              <div className={styles.PictureWrapper}>
                <ProfilePicture picture={isMine ? userGeneralInfo?.picture : userInfo.picture} />
              </div>
              <div className={styles.Name}>
                {userInfo.name}
              </div>
            </div>
            <div className={styles.Actions}>
              {isMine
              && (
              <Button className={styles.DeleteButton} onClick={() => setIsDeleteModalOpen(true)}>
                Delete Account
              </Button>
              )}
            </div>
          </div>
          <div className={styles.Sections}>
            <div className={styles.Section}>
              <EditableText
                title="Personal information"
                isMine={isMine}
                onChange={onDescriptionChange}
                realDescription={userInfo.description}
              />
            </div>
            <div className={styles.Section}>
              <div className={styles.SectionTitle}>
                Most uploaded genre
              </div>
              {userInfo.genres ? <GenresLink genres={userInfo.genres} route={pageRoutes.SONGS} /> : 'This user has not uploaded any songs yet'}
            </div>
          </div>
        </div>
      </div>
      {userInfo?.name
      && (isMine ? (
        <MySongs />
      )
        : (
          <Songs username={userInfo.name} />
        )
      )}
      {isDeleteModalOpen && (
      <DeleteAccountModal
        isOpen={isDeleteModalOpen}
        onClose={() => setIsDeleteModalOpen(false)}
        onDelete={() => setUserInfo({})}
      />
      )}
    </div>
  );
}
