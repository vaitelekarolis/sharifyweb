/* eslint-disable react/jsx-props-no-spreading */
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import { email } from '../../constants/regex';
import toastService from '../../services/toast';
import userService from '../../services/user';
import styles from '../../styles/user/SignUpForm.module.scss';

export default function SignUp() {
  const history = useHistory();
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [dirty, setDirty] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!isEmailValid) {
      toastService.error('Invalid email format');
      return;
    }
    const data = new FormData(event.currentTarget);
    const result = await userService.registerAsync(data.get('username'), data.get('email'), data.get('password'));
    if (result) {
      toastService.success('Account has been successfully created');
      history.push(pageRoutes.LOGIN);
    }
  };

  const onEmailChange = (e) => {
    const { value } = e.target;
    if (email.test(value)) {
      setIsEmailValid(true);
    } else {
      setIsEmailValid(false);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={dirty && !isEmailValid}
                onBlur={() => setDirty(true)}
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={onEmailChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="new-password"
              />
            </Grid>
            <div className={styles.Hint}>
              <span>
                {'* '}
              </span>
              Password must be 8 characters of length,
              have uppercase letter and at least one digit
            </div>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="center">
            <Grid item>
              <Link to={pageRoutes.LOGIN} style={{ color: '#42424a', textDecoration: 'underline', textDecorationColor: '#42424a' }}>
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
