import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import PropTypes from 'prop-types';
import styles from '../styles/LikeButton.module.scss';

export default function LikeButton({ isLiked, toggleLike }) {
  return (
    isLiked
      ? <FavoriteIcon className={styles.LikedIcon} onClick={toggleLike} />
      : <FavoriteBorderIcon className={styles.LikeIcon} onClick={toggleLike} />
  );
}

LikeButton.propTypes = {
  isLiked: PropTypes.bool.isRequired,
  toggleLike: PropTypes.func.isRequired,
};
