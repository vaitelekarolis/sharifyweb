import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import styles from '../styles/LinkWithIcon.module.scss';

export default function LinkWithIcon({ route, children }) {
  const location = useLocation();

  return (
    <Link
      to={route}
      className={location.pathname.startsWith(route) ? styles.ActiveLink : styles.IconLink}
    >
      {children}
    </Link>
  );
}

LinkWithIcon.propTypes = {
  route: PropTypes.string.isRequired,
  children: PropTypes.node,
};

LinkWithIcon.defaultProps = {
  children: null,
};
