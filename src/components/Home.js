import HomeIcon from '@mui/icons-material/Home';
import { useContext, useEffect, useState } from 'react';
import { pageRoutes } from '../constants/pageRoutes';
import localStorageService from '../services/localStorage';
import songService from '../services/song';
import { UserGeneralInfo } from './ContextProvider';
import PageTitle from './PageTitle';
import SongList from './song/SongList';

export default function Home() {
  const [myMostPlayedSongs, setMyMostPlayedSongs] = useState([]);
  const [topPlayedSongs, setTopPlayedSongs] = useState([]);
  const [newestSongs, setNewestSongs] = useState([]);
  const { userGeneralInfo } = useContext(UserGeneralInfo);

  useEffect(() => {
    if (userGeneralInfo?.name && localStorageService.isLoggedIn()) {
      songService.getCreatedByAsync({}, userGeneralInfo.name, 5)
        .then((result) => {
          setMyMostPlayedSongs(result);
        });
    } else {
      setMyMostPlayedSongs([]);
    }

    songService.getTopPlayedAsync()
      .then((result) => {
        if (result) {
          setTopPlayedSongs(result);
        }
      });

    songService.getNewestAsync()
      .then((result) => {
        if (result) {
          setNewestSongs(result);
        }
      });
  }, [userGeneralInfo]);

  return (
    <div className="PageWrapper">
      <PageTitle title="Home" Icon={HomeIcon} hint="East or West, home's the best" />
      {Boolean(myMostPlayedSongs.length) && (
      <SongList
        songs={myMostPlayedSongs}
        setSongs={setMyMostPlayedSongs}
        title="Your most popular uploads"
        viewAll={`${pageRoutes.PROFILE}/${userGeneralInfo.name}`}
      />
      )}
      <SongList
        songs={topPlayedSongs}
        setSongs={setTopPlayedSongs}
        title="Most played songs"
        viewAll={pageRoutes.SONGS}
      />
      <SongList
        songs={newestSongs}
        setSongs={setNewestSongs}
        title="Most recent songs"
        viewAll={pageRoutes.SONGS}
      />
    </div>
  );
}
