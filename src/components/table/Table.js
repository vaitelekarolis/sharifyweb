import { Grid } from '@mui/material';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from '../../styles/table/Table.module.scss';

export default function Table({
  title, columns, rows, viewAll, onDataDoubleClick, selected
}) {
  const flexAlignByAlign = {
    left: 'flex-start',
    center: 'center',
    right: 'flex-end',
  };

  const isSelected = (id) => (
    selected === id
  );

  return (
    <div className={styles.Wrapper}>
      <div className={styles.Title}>{title}</div>
      {viewAll && (
      <Link className={styles.ViewAll} to={viewAll}>
        {'View all >>'}
      </Link>
      )}
      <Grid container className={styles.Table}>
        <Grid
          container
          item
          xs={12}
          className={styles.Header}
          justifyContent="space-between"
          direction="row"
        >
          {columns.map((column) => (
            <Grid
              key={column.id}
              item
              container
              xs={column.xs || Math.floor(12 / columns.length)}
              textAlign={column.align || 'left'}
              style={{ justifyContent: flexAlignByAlign[column.align] || 'flex-start' }}
            >
              {column.header.toUpperCase()}
            </Grid>
          ))}
        </Grid>

        {rows.map((row) => (
          <Grid
            key={row.id}
            container
            item
            xs={12}
            className={isSelected(row.id) ? styles.SelectedDataRow : styles.DataRow}
            justifyContent="space-between"
            direction="row"
            onDoubleClick={(e) => onDataDoubleClick(e, row.id)}
          >
            {columns.map((column) => (
              <Grid
                key={column.id}
                item
                container
                xs={column.xs || Math.floor(12 / columns.length)}
                textAlign={column.align || 'left'}
                style={{ justifyContent: flexAlignByAlign[column.align] || 'flex-start', alignItems: 'center' }}
              >
                {row[column.id]}
              </Grid>
            ))}
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

Table.propTypes = {
  title: PropTypes.string.isRequired,
  columns: PropTypes.instanceOf(Array).isRequired,
  rows: PropTypes.instanceOf(Array).isRequired,
  viewAll: PropTypes.string,
  onDataDoubleClick: PropTypes.func,
  selected: PropTypes.string,
};

Table.defaultProps = {
  viewAll: null,
  onDataDoubleClick: () => {},
  selected: null,
};
