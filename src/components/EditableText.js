import EditIcon from '@mui/icons-material/Edit';
import { Button, TextField } from '@mui/material';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import styles from '../styles/EditableText.module.scss';

export default function EditableText({
  title, isMine, onChange, realDescription
}) {
  const [description, setDescription] = useState('');
  const [isEditing, setIsEditing] = useState(false);

  const onDescriptionChange = async () => {
    await onChange(description);
    setIsEditing(false);
  };

  useEffect(() => {
    if (isEditing) {
      setDescription(realDescription);
    } else {
      setDescription('');
    }
  }, [isEditing]);

  return (
    <>
      <div className={styles.SectionTitle}>
        {title}
        {isMine
                && (
                <EditIcon
                  className={styles.Icon}
                  onClick={() => setIsEditing(!isEditing)}
                />
                )}
      </div>
      <div>
        {isEditing && isMine
          ? (
            <>
              <TextField
                multiline
                rows={4}
                fullWidth
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
              <div className={styles.ConfirmWrapper}>
                <Button
                  onClick={onDescriptionChange}
                  disabled={description === realDescription
          || (!realDescription && !description)}
                >
                  Confirm
                </Button>
              </div>
            </>
          )
          : realDescription}
      </div>
    </>
  );
}

EditableText.propTypes = {
  title: PropTypes.string.isRequired,
  isMine: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  realDescription: PropTypes.string.isRequired,
};
