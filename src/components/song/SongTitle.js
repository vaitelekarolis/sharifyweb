import MusicNoteIcon from '@mui/icons-material/MusicNote';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import imageService from '../../services/image';
import styles from '../../styles/song/SongTitle.module.scss';
import { Player } from '../ContextProvider';

export default function SongTitle({ song, isFooter }) {
  const { currentSongId } = useContext(Player);

  return (
    <div className={styles.Wrapper}>
      <div className={styles.PictureWrapper}>
        {song.picture ? (
          <img
            src={imageService.getImageFromBase64(song.picture)}
            alt={song.author}
            className={styles.SongPicture}
          />
        ) : (
          <MusicNoteIcon
            className={styles.PicturePlaceholder}
          />
        )}
      </div>
      <div>
        <div className={isFooter ? styles.FooterSongTitle : `${styles.SongTitle} ${currentSongId === song.id ? styles.Playing : ''}`}>
          {song.title}
        </div>
        <div className={isFooter ? styles.FooterSongAuthor : styles.SongAuthor}>
          <Link to={`${pageRoutes.PROFILE}/${song.author}`}>
            {song.author}
          </Link>
        </div>
      </div>
    </div>
  );
}

SongTitle.propTypes = {
  song: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    picture: PropTypes.string,
  }).isRequired,
  isFooter: PropTypes.bool,
};

SongTitle.defaultProps = {
  isFooter: false,
};
