import AddBoxIcon from '@mui/icons-material/AddBox';
import { useContext, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import songService from '../../services/song';
import { Player } from '../ContextProvider';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import Table from '../table/Table';
import SongRequestActions from './SongRequestActions';
import SongStatus from './SongStatus';
import SongTitle from './SongTitle';

export default function SongRequests() {
  const [songRequests, setSongRequests] = useState([]);

  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    songService.getSongRequestsAsync, setSongRequests
  );

  const { currentSongId, startPlay, setAutoPlay } = useContext(Player);

  const playSong = (e, songId) => {
    e.stopPropagation();
    startPlay(songId);
    setAutoPlay(true);
  };

  const updateSongStatus = (id, status) => {
    const newSongRequests = songRequests.map((song) => (song.id === id ? {
      ...song,
      status
    } : song));
    setSongRequests(newSongRequests);
  };

  const columns = [
    {
      id: 'nr', header: '#', xs: 1, align: 'center'
    },
    {
      id: 'song', header: 'song', xs: 4, align: 'left'
    },
    { id: 'genre', header: 'genre', align: 'left' },
    { id: 'status', header: 'status', align: 'center' },
    { id: 'action', header: 'action', align: 'center' },
  ];

  const rows = songRequests.map((songRequest, index) => (
    {
      id: songRequest.id,
      nr: index + 1,
      song: <SongTitle song={songRequest} />,
      genre: songRequest.genre,
      status: <SongStatus status={songRequest.status} />,
      action: <SongRequestActions
        songRequest={songRequest}
        updateSongStatus={(status) => updateSongStatus(songRequest.id, status)}
      />
    }
  ));

  return (
    <div className="PageWrapper">
      <PageTitle title="Song uploads" Icon={AddBoxIcon} hint="List of song upload requests that need your approval." />
      <Filter setIsLoading={setIsLoading} hasStatus />
      {songRequests && songRequests.length !== 0
        ? (
          <>
            <InfiniteScroll
              dataLength={songRequests.length}
              loadMore={nextPage}
              hasMore={!isLoading}
              threshold={150}
            >
              <div className="PageTableWrapper">
                <Table title="Song requests" columns={columns} rows={rows} onDataDoubleClick={playSong} selected={currentSongId} />
              </div>
            </InfiniteScroll>
          </>
        ) : (
          <div className="EmptyPage">
            No songs are uploaded for review
          </div>
        )}
    </div>
  );
}
