import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
import { pageRoutes } from '../../constants/pageRoutes';
import localStorageService from '../../services/localStorage';
import playlistService from '../../services/playlist';
import toastService from '../../services/toast';
import styles from '../../styles/song/SongList.module.scss';
import { Player } from '../ContextProvider';
import CreateNewButton from '../CreateNewButton';
import GenresLink from '../GenresLink';
import Table from '../table/Table';
import CreateSongModal from './CreateSongModal';
import SongActions from './SongActions';
import SongIndex from './SongIndex';
import SongTitle from './SongTitle';

export default function SongList({
  songs, setSongs, isMine, playlistId, title, canAdd, viewAll
}) {
  const [isCreateSongModalOpen, setIsCreateSongModalOpen] = useState(false);
  const { currentSongId, startPlay, setAutoPlay } = useContext(Player);

  const playSong = (e, songId) => {
    e.stopPropagation();
    startPlay(songId, playlistId);
    setAutoPlay(true);
  };

  const removeSong = (songId) => {
    setSongs((prevSongs) => prevSongs.filter((song) => song.id !== songId));
  };

  const removeSongAsync = async (song) => {
    const result = await playlistService.removeSongAsync(playlistId, song.id);
    if (result) {
      removeSong(song.id);
      toastService.info(`Song ${song.title} removed from playlist`);
    }
  };

  const columns = [
    {
      id: 'nr', header: '#', xs: 1, align: 'center'
    },
    {
      id: 'song', header: 'song', xs: 4, align: 'left'
    },
    { id: 'genre', header: 'genre', align: 'left' },
    { id: 'listenCount', header: 'listen count', align: 'center' },
    { id: 'action', header: 'action', align: 'center' },
  ];

  const rows = songs.map((song, index) => (
    {
      id: song.id,
      nr: <SongIndex songId={song.id} index={index + 1} />,
      song: <SongTitle song={song} />,
      genre: <GenresLink genres={[song.genre]} route={pageRoutes.SONGS} />,
      listenCount: song.listenCount.toLocaleString(),
      action: <SongActions
        song={song}
        key={song.id}
        isMine={isMine}
        onRemove={isMine ? () => removeSongAsync(song) : null}
        playlistId={playlistId}
        removeSong={() => removeSong(song.id)}
      />
    }
  ));

  return (
    <>
      <div className="PageTableWrapper">
        <div className={styles.Actions}>
          <div className={styles.UploadButton}>
            {localStorageService.isLoggedIn() && canAdd && (
            <CreateNewButton onClick={() => setIsCreateSongModalOpen(true)} />
            )}
          </div>
        </div>
        <Table title={title || 'Songs'} columns={columns} rows={rows} viewAll={viewAll} onDataDoubleClick={playSong} selected={currentSongId} />
      </div>
      {isCreateSongModalOpen
      && (
      <CreateSongModal
        isOpen={isCreateSongModalOpen}
        onClose={() => setIsCreateSongModalOpen(false)}
      />
      )}
    </>
  );
}

SongList.propTypes = {
  songs: PropTypes.instanceOf(Array).isRequired,
  setSongs: PropTypes.func.isRequired,
  isMine: PropTypes.bool,
  playlistId: PropTypes.string,
  title: PropTypes.string,
  canAdd: PropTypes.bool,
  viewAll: PropTypes.string,
};

SongList.defaultProps = {
  isMine: false,
  playlistId: null,
  title: 'Songs',
  canAdd: false,
  viewAll: null,
};
