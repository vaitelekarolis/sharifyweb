import MusicNoteIcon from '@mui/icons-material/MusicNote';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import songService from '../../services/song';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import SongList from './SongList';

export default function AllSongs() {
  const [songs, setSongs] = useState([]);
  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    songService.getAllAsync, setSongs
  );

  return (
    <div className="PageWrapper">
      <PageTitle title="All songs" Icon={MusicNoteIcon} hint="You can find the list of all songs here." />
      <Filter setIsLoading={setIsLoading} />
      {Boolean(songs.length)
        && (
        <InfiniteScroll
          dataLength={songs.length}
          loadMore={nextPage}
          hasMore={!isLoading}
          threshold={150}
        >
          <SongList songs={songs} setSongs={setSongs} canAdd />
        </InfiniteScroll>
        )}
    </div>
  );
}
