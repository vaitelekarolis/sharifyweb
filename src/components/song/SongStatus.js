import PropTypes from 'prop-types';
import styles from '../../styles/song/SongStatus.module.scss';

export default function SongStatus({ status }) {
  return (
    <div className={`${styles[status]}`}>
      {status.toLocaleUpperCase()}
    </div>
  );
}

SongStatus.propTypes = {
  status: PropTypes.string.isRequired,
};
