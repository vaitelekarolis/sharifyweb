import { makeStyles } from '@material-ui/core';
import {
  Box, Button, FormControl, InputLabel, MenuItem, Select, TextField
} from '@mui/material';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { FileUploader } from 'react-drag-drop-files';
import fileService from '../../services/file';
import songService from '../../services/song';
import toastService from '../../services/toast';
import BasicModal from '../BasicModal';

const useStyles = makeStyles(() => ({
  menuPaper: {
    maxHeight: 300,
  }
}));

export default function CreateSongModal({ isOpen, onClose }) {
  const styles = useStyles();
  const genreId = 'genreId';

  const [title, setTitle] = useState('');
  const [genre, setGenre] = useState('');
  const [file, setFile] = useState();

  const [genres, setGenres] = useState([]);

  const onSubmitAsync = async (e) => {
    e.preventDefault();
    let songBase64;
    try {
      songBase64 = await fileService.convertToBase64(file);
    } catch {
      toastService.error('You must upload valid file');
      return;
    }
    const fileType = file.type;
    const fileName = file.name;

    const result = await songService.createAsync({
      title, genre, songBase64, fileType, fileName
    });
    if (result) {
      toastService.success(`Created song ${title}`);
      onClose();
    }
  };

  const onFileUpload = (uploadedFile) => {
    setFile(uploadedFile);
  };

  useEffect(async () => {
    const genreList = await songService.getGenresAsync();
    setGenres(genreList);
  }, []);

  const renderGenres = () => (
    genres.map((genreChoice) => (
      <MenuItem key={genreChoice} value={genreChoice}>{genreChoice}</MenuItem>
    ))
  );

  return (
    <BasicModal isOpen={isOpen} onClose={onClose}>
      <div className="ModalTitle">
        Upload Song
      </div>
      <Box component="form" className="CreateForm" onSubmit={onSubmitAsync}>
        <FormControl>
          <TextField
            required
            label="Title"
            variant="outlined"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </FormControl>
        <FormControl fullWidth>
          <InputLabel id={genreId} required>Genre</InputLabel>
          <Select
            labelId={genreId}
            id="genre"
            value={genre}
            label="Genre"
            onChange={(e) => setGenre(e.target.value)}
            style={{ textAlign: 'left' }}
            MenuProps={{ classes: { paper: styles.menuPaper } }}
            required
          >
            <MenuItem value=""><i>Blank</i></MenuItem>
            {renderGenres()}
          </Select>
        </FormControl>
        <FormControl>
          <FileUploader
            name="Song"
            handleChange={onFileUpload}
            maxSize={10}
            onSizeError={(error) => toastService.error(error)}
            onTypeError={(error) => toastService.error(error)}
            types={['mp3', 'wav', 'wma', 'ogg', 'flac', 'aiff', 'aac']}
            label="Upload here"
          />
        </FormControl>
        <Button type="submit">
          Upload Song
        </Button>
      </Box>
    </BasicModal>
  );
}

CreateSongModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
