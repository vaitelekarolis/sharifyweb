import { Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import songService from '../../services/song';
import styles from '../../styles/song/Song.module.scss';
import PlaySongButton from './PlaySongButton';

export default function Song() {
  const { id } = useParams();
  const [song, setSong] = useState({});

  useEffect(async () => {
    const result = await songService.getByIdAsync(id);
    if (!result.error) {
      setSong(result);
    }
  }, []);

  return (
    <div className="PageWrapper">
      <div className="PageTitle">
        {song.title}
        <span className={styles.PlaySongButton}>
          <PlaySongButton song={song} />
        </span>
      </div>
      <div className="PageSubtitle">
        {song.author}
      </div>
      <Grid container className={styles.SongDetails}>
        <Grid item xs={2} className={styles.SongDetailsRow}>
          <div>
            Album:
          </div>
          <div>
            Genre:
          </div>
        </Grid>
        <Grid item xs={10} className={styles.SongDetailsRowValues}>
          <div>
            {song.album}
          </div>
          <div>
            {song.genre}
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
