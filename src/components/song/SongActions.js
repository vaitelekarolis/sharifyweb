/* eslint-disable max-lines */
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import { Menu, MenuItem } from '@mui/material';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import songStatuses from '../../constants/songStatuses';
import localStorageService from '../../services/localStorage';
import playlistService from '../../services/playlist';
import songService from '../../services/song';
import toastService from '../../services/toast';
import styles from '../../styles/song/SongActions.module.scss';
import { UserGeneralInfo } from '../ContextProvider';
import LikeButton from '../LikeButton';
import DeleteSongModal from './DeleteSongModal';
import PlaySongButton from './PlaySongButton';

export default function SongActions({
  song, isMine, onRemove, playlistId, removeSong
}) {
  const history = useHistory();
  const { userGeneralInfo } = useContext(UserGeneralInfo);
  const [playlistMenu, setPlaylistMenu] = useState(null);
  const [actionsMenu, setActionsMenu] = useState(null);
  const [myPlaylists, setMyPlaylists] = useState([]);
  const [isLiked, setIsLiked] = useState(song.isLiked || false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const playlistMenuOpen = Boolean(playlistMenu);
  const actionsMenuOpen = Boolean(actionsMenu);

  const isSongMine = song.author === userGeneralInfo.name;

  const handleAddClick = (e) => {
    e.stopPropagation();
    setPlaylistMenu(e.currentTarget);
  };

  const handleAddClose = (e) => {
    e.stopPropagation();
    setPlaylistMenu(null);
  };

  const handleActionOpen = (e) => {
    e.stopPropagation();
    setActionsMenu(e.target);
  };

  const handleActionClose = (e) => {
    e.stopPropagation();
    setActionsMenu(null);
  };

  const handleRemoveFromPlaylist = () => {
    onRemove();
    setActionsMenu(null);
  };

  const updatePlaylistsAsync = async () => {
    const result = await playlistService.getSongsPlaylistsAsync(song.id);

    setMyPlaylists(result.playlists);
  };

  const addSongAsync = async (e, playlist) => {
    const result = await playlistService.addSongAsync(playlist.id, song.id);
    if (result) {
      toastService.success(`Successfully added '${song.title}' to '${playlist.name}'`);
      await updatePlaylistsAsync();
      setPlaylistMenu(null);
    }
  };

  const toggleLikeAsync = async (e) => {
    e.stopPropagation();
    const result = await songService.toggleLikeAsync(song.id);
    if (result) {
      setIsLiked(!isLiked);
      setActionsMenu(null);
    }
  };

  const openAuthorProfile = () => {
    history.push(`${pageRoutes.PROFILE}/${song.author}`);
  };

  const openDeleteSongModal = () => {
    setActionsMenu(null);
    setIsDeleteModalOpen(true);
  };

  useEffect(async () => {
    if (localStorageService.isLoggedIn()) {
      await updatePlaylistsAsync();
    }
  }, []);

  const renderPlaylists = () => (
    myPlaylists.map((myPlaylist) => (
      <MenuItem
        key={myPlaylist.id}
        value={myPlaylist.id}
        onClick={(e) => addSongAsync(e, myPlaylist)}
        disabled={myPlaylist.hasSong}
      >
        {myPlaylist.name}
      </MenuItem>
    ))
  );

  if (!localStorageService.isLoggedIn()) {
    return <PlaySongButton song={song} playlistId={playlistId} />;
  }

  return (
    <div className={styles.Actions}>
      <div>
        {(song.status === songStatuses.CONFIRMED
          || !song.status)
          && (
          <>
            <PlaylistAddIcon onClick={handleAddClick} className={styles.AddToPlaylistButton} />
            <Menu
              id="playlist-menu"
              anchorEl={playlistMenu}
              open={playlistMenuOpen}
              onClose={handleAddClose}
              PaperProps={{
                style: {
                  maxHeight: 200,
                },
              }}
            >
              {myPlaylists.length === 0 ? (
                <div style={{ padding: '0 5px' }}>
                  You do not have any playlists
                </div>
              ) : renderPlaylists()}
            </Menu>
          </>
          )}
      </div>

      <div>
        {(song.status === songStatuses.CONFIRMED
          || !song.status)
            && (
            <LikeButton
              isLiked={isLiked}
              toggleLike={toggleLikeAsync}
            />
            )}
      </div>

      <div>
        <MoreHorizIcon onClick={handleActionOpen} className={styles.Icon} />
        <Menu
          id="actions-menu"
          anchorEl={actionsMenu}
          open={actionsMenuOpen}
          onClose={handleActionClose}
          PaperProps={{
            style: {
              maxHeight: 200,
            },
          }}
        >
          {isMine && (
            <MenuItem onClick={handleRemoveFromPlaylist}>
              Remove from playlist
            </MenuItem>
          )}
          <MenuItem onClick={openAuthorProfile}>
            {'Go to author\'s page'}
          </MenuItem>
          {isSongMine && (
            <MenuItem onClick={openDeleteSongModal}>
              Delete song
            </MenuItem>
          )}
        </Menu>
      </div>

      <PlaySongButton song={song} playlistId={playlistId} />
      {isDeleteModalOpen
      && (
      <DeleteSongModal
        song={song}
        isOpen={isDeleteModalOpen}
        onClose={() => setIsDeleteModalOpen(false)}
        removeSong={removeSong}
      />
      )}
    </div>
  );
}

SongActions.propTypes = {
  song: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    album: PropTypes.string,
    genre: PropTypes.string,
    addedBy: PropTypes.string,
    songName: PropTypes.string,
    isLiked: PropTypes.bool,
    status: PropTypes.string,
  }).isRequired,
  isMine: PropTypes.bool,
  onRemove: PropTypes.func,
  playlistId: PropTypes.string,
  removeSong: PropTypes.func,
};

SongActions.defaultProps = {
  isMine: false,
  onRemove: () => {},
  playlistId: null,
  removeSong: () => {},
};
