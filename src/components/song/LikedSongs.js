import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import songService from '../../services/song';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import SongList from './SongList';

export default function LikedSongs() {
  const [songs, setSongs] = useState([]);
  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    songService.getLikedAsync, setSongs
  );

  return (
    <div className="PageWrapper">
      <PageTitle title="Liked songs" Icon={FavoriteBorder} hint="The list of your most favoured songs." />
      <Filter setIsLoading={setIsLoading} />
      {songs.length
        ? (
          <InfiniteScroll
            dataLength={songs.length}
            loadMore={nextPage}
            hasMore={!isLoading}
            threshold={150}
          >
            <SongList songs={songs} setSongs={setSongs} title="Liked songs" canAdd />
          </InfiniteScroll>
        ) : (
          <div className="EmptyPage">
            You have not liked any songs yet
          </div>
        )}
    </div>
  );
}
