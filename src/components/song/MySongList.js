import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
import localStorageService from '../../services/localStorage';
import styles from '../../styles/song/SongList.module.scss';
import { Player } from '../ContextProvider';
import CreateNewButton from '../CreateNewButton';
import Table from '../table/Table';
import CreateSongModal from './CreateSongModal';
import SongActions from './SongActions';
import SongIndex from './SongIndex';
import SongStatus from './SongStatus';
import SongTitle from './SongTitle';

export default function MySongList({
  songs, title,
}) {
  const [isCreateSongModalOpen, setIsCreateSongModalOpen] = useState(false);
  const { currentSongId, startPlay, setAutoPlay } = useContext(Player);

  const playSong = (e, songId) => {
    e.stopPropagation();
    startPlay(songId);
    setAutoPlay(true);
  };

  const columns = [
    {
      id: 'nr', header: '#', xs: 1, align: 'center'
    },
    {
      id: 'song', header: 'song', xs: 4, align: 'left'
    },
    { id: 'genre', header: 'genre', align: 'left' },
    { id: 'status', header: 'status', align: 'center' },
    { id: 'action', header: 'action', align: 'center' },
  ];

  const rows = songs.map((song, index) => (
    {
      id: song.id,
      nr: <SongIndex songId={song.id} index={index + 1} />,
      song: <SongTitle song={song} />,
      genre: song.genre,
      status: <SongStatus status={song.status} />,
      action: <SongActions
        song={song}
        key={song.id}
      />
    }
  ));

  return (
    <div className="PageTableWrapper">
      <div className={styles.Actions}>
        <div className={styles.UploadButton}>
          {localStorageService.isLoggedIn() && (
            <CreateNewButton onClick={() => setIsCreateSongModalOpen(true)} />
          )}
        </div>
      </div>
      <Table title={title || 'Songs'} columns={columns} rows={rows} onDataDoubleClick={playSong} selected={currentSongId} />
      {isCreateSongModalOpen
      && (
      <CreateSongModal
        isOpen={isCreateSongModalOpen}
        onClose={() => setIsCreateSongModalOpen(false)}
      />
      )}
    </div>
  );
}

MySongList.propTypes = {
  songs: PropTypes.instanceOf(Array).isRequired,
  title: PropTypes.string,
};

MySongList.defaultProps = {
  title: 'Songs',
};
