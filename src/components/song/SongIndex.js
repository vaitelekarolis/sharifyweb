import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import styles from '../../styles/song/SongIndex.module.scss';
import { Player } from '../ContextProvider';

export default function SongIndex({ songId, index }) {
  const { currentSongId, isPlaying } = useContext(Player);

  const isCurrentSong = currentSongId === songId;
  return (
    <div className={isCurrentSong ? styles.PlayingWrapper : styles.Wrapper}>
      {isCurrentSong && isPlaying
        ? <VolumeUpIcon className={styles.Icon} />
        : index}
    </div>
  );
}

SongIndex.propTypes = {
  songId: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};
