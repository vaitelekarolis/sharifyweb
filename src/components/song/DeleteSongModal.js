import { Button } from '@mui/material';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import songService from '../../services/song';
import toastService from '../../services/toast';
import styles from '../../styles/song/DeleteSongModal.module.scss';
import BasicModal from '../BasicModal';
import { Player } from '../ContextProvider';

export default function DeleteSongModal({
  song, isOpen, onClose, removeSong
}) {
  const {
    clearPlayer, currentSongId, previousSongId, nextSongId
  } = useContext(Player);

  const deleteSongAsync = async () => {
    const result = await songService.deleteSongAsync(song.id);

    if (result) {
      onClose();
      removeSong();
      if (currentSongId === song.id || previousSongId === song.id || nextSongId === song.id) {
        clearPlayer();
      }
      toastService.info(`Song '${song.title}' has been deleted`);
    }
  };

  return (
    <BasicModal isOpen={isOpen} onClose={onClose}>
      <div className={styles.Wrapper}>
        <div className={styles.Text}>
          Are you sure you want to delete the song
          {' '}
          <span className={styles.Highlight}>{song.title}</span>
          ?
          <br />
          Once deleted you cannot undo this action.
        </div>
        <div className={styles.Buttons}>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button className={styles.DeleteButton} onClick={deleteSongAsync}>
            Delete
          </Button>
        </div>
      </div>
    </BasicModal>
  );
}

DeleteSongModal.propTypes = {
  song: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
  }).isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  removeSong: PropTypes.func.isRequired,
};
