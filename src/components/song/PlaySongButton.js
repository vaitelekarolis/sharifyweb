import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import styles from '../../styles/song/PlaySongButton.module.scss';
import { Player } from '../ContextProvider';

export default function PlaySongButton({ song, playlistId }) {
  const { startPlay, setAutoPlay } = useContext(Player);

  const playSong = (e) => {
    e.stopPropagation();
    startPlay(song.id, playlistId);
    setAutoPlay(true);
  };

  return (
    <PlayArrowIcon className={styles.Icon} onClick={playSong} />
  );
}

PlaySongButton.propTypes = {
  song: PropTypes.instanceOf(Object).isRequired,
  playlistId: PropTypes.string,
};

PlaySongButton.defaultProps = {
  playlistId: null,
};
