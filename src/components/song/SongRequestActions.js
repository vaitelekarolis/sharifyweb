import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import songStatuses from '../../constants/songStatuses';
import songService from '../../services/song';
import toastService from '../../services/toast';
import styles from '../../styles/song/SongRequestActions.module.scss';
import { Player } from '../ContextProvider';
import PlaySongButton from './PlaySongButton';

export default function SongRequestActions({ songRequest, updateSongStatus }) {
  const { currentSongId, clearPlayer } = useContext(Player);

  const approveSongAsync = async () => {
    const result = await songService.confirmSongAsync(songRequest.id);
    if (result) {
      updateSongStatus(songStatuses.CONFIRMED);
      toastService.success(`Approved ${songRequest.title} by ${songRequest.author}`);
    }
  };

  const rejectSongAsync = async () => {
    const result = await songService.rejectSongAsync(songRequest.id);
    if (result) {
      updateSongStatus(songStatuses.REJECTED);
      if (currentSongId === songRequest.id) {
        clearPlayer();
      }
      toastService.info(`Rejected ${songRequest.title} by ${songRequest.author}`);
    }
  };

  return (
    <div className={styles.Actions}>
      <div className={styles.RequestActions}>
        <div>
          {songRequest.status !== songStatuses.CONFIRMED
        && <CheckCircleIcon onClick={approveSongAsync} className={styles.ApproveIcon} />}
        </div>
        <div>
          {songRequest.status !== songStatuses.REJECTED
        && <RemoveCircleIcon onClick={rejectSongAsync} className={styles.RejectIcon} />}
        </div>
      </div>
      <PlaySongButton song={songRequest} />
    </div>
  );
}

SongRequestActions.propTypes = {
  songRequest: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    album: PropTypes.string,
    genre: PropTypes.string,
    addedBy: PropTypes.string,
    reference: PropTypes.string,
    status: PropTypes.string,
  }).isRequired,
  updateSongStatus: PropTypes.func.isRequired,
};
