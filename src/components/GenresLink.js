import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from '../styles/GenresLink.module.scss';

export default function GenresLink({ genres, route }) {
  if (!genres || !genres.length) {
    return null;
  }

  return genres.map((genre) => (
    <Link to={`${route}?genre=${genre}`} className={styles.Link}>
      {genre}
    </Link>
  )).reduce((prev, curr) => [prev, <span>,&nbsp;</span>, curr]);
}

GenresLink.propTypes = {
  genres: PropTypes.arrayOf(PropTypes.string).isRequired,
  route: PropTypes.string.isRequired,
};
