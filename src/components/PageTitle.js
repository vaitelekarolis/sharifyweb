import PropTypes from 'prop-types';
import styles from '../styles/PageTitle.module.scss';

export default function PageTitle({ title, Icon, hint }) {
  return (
    <div className={styles.Wrapper}>
      <div className={styles.IconWrapper}>
        <Icon className={styles.Icon} />
      </div>
      <div className={styles.MainWrapper}>
        <div className={styles.Title}>
          {title}
        </div>
        <div className={styles.Hint}>
          {hint}
        </div>
      </div>
    </div>
  );
}

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
  Icon: PropTypes.node.isRequired,
  hint: PropTypes.string.isRequired,
};
