import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import styles from '../../styles/song/SongIndex.module.scss';
import { Player } from '../ContextProvider';

export default function PlaylistIndex({ id, index }) {
  const { playlistId, isPlaying } = useContext(Player);
  const isCurrentPlaylist = playlistId === id;

  return (
    <div className={isCurrentPlaylist ? styles.PlayingWrapper : styles.Wrapper}>
      {isCurrentPlaylist && isPlaying
        ? <VolumeUpIcon className={styles.Icon} />
        : index}
    </div>
  );
}

PlaylistIndex.propTypes = {
  id: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};
