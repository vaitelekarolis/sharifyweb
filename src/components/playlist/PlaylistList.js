import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import styles from '../../styles/song/SongList.module.scss';
import { Player } from '../ContextProvider';
import CreateNewButton from '../CreateNewButton';
import GenresLink from '../GenresLink';
import Table from '../table/Table';
import CreatePlaylistModal from './CreatePlaylistModal';
import PlaylistActions from './PlaylistActions';
import PlaylistIndex from './PlaylistIndex';
import PlaylistTitle from './PlaylistTitle';

export default function PlaylistList({ playlists, setPlaylists, title }) {
  const [isCreatePlaylistModalOpen, setIsCreatePlaylistModalOpen] = useState(false);
  const { playlistId, startPlay, setAutoPlay } = useContext(Player);

  const playSong = (e, id) => {
    const playlist = playlists.find((plst) => plst.id === id);
    e.stopPropagation();
    if (playlist.firstSongId) {
      startPlay(playlist.firstSongId, id);
      setAutoPlay(true);
    }
  };

  const updateLikes = (plstId, isLiked) => {
    const newPlaylists = playlists.map((playlist) => (playlist.id === plstId
      ? {
        ...playlist,
        likeCount: isLiked
          ? playlist.likeCount + 1
          : playlist.likeCount - 1
      }
      : playlist));

    setPlaylists(newPlaylists);
  };

  const removePlaylist = (plstId) => {
    setPlaylists((prevPlaylists) => prevPlaylists.filter((playlist) => playlist.id !== plstId));
  };

  const columns = [
    {
      id: 'nr', header: '#', xs: 1, align: 'center'
    },
    {
      id: 'name', header: 'name', xs: 4, align: 'left'
    },
    {
      id: 'genre', header: 'genre', xs: 4, align: 'left'
    },
    {
      id: 'likes', header: 'likes', xs: 1, align: 'center'
    },
    { id: 'action', header: 'action', align: 'center' },
  ];

  const rows = playlists.map((playlist, index) => (
    {
      id: playlist.id,
      nr: <PlaylistIndex id={playlist.id} index={index + 1} />,
      name: <PlaylistTitle id={playlist.id}><Link to={`${pageRoutes.PLAYLISTS}/${playlist.id}`}>{playlist.name}</Link></PlaylistTitle>,
      genre: <GenresLink genres={playlist.genres} route={pageRoutes.PLAYLISTS} />,
      likes: playlist.likeCount,
      action: <PlaylistActions
        playlist={playlist}
        updateLikes={(isLiked) => updateLikes(playlist.id, isLiked)}
        removePlaylist={() => removePlaylist(playlist.id)}
      />
    }
  ));

  return (
    <div className="PageTableWrapper">
      <div className={styles.UploadButton}>
        <CreateNewButton onClick={() => setIsCreatePlaylistModalOpen(true)} text="Create new" />
      </div>
      {playlists && playlists.length !== 0
        ? (
          <Table title={title || 'Playlists'} columns={columns} rows={rows} onDataDoubleClick={playSong} selected={playlistId} />
        )
        : (
          <div className="EmptyPage">
            No playlists are present
          </div>
        )}
      <CreatePlaylistModal
        isOpen={isCreatePlaylistModalOpen}
        onClose={() => setIsCreatePlaylistModalOpen(false)}
      />
    </div>
  );
}

PlaylistList.propTypes = {
  playlists: PropTypes.instanceOf(Array).isRequired,
  setPlaylists: PropTypes.func.isRequired,
  title: PropTypes.string,
};

PlaylistList.defaultProps = {
  title: 'Playlists',
};
