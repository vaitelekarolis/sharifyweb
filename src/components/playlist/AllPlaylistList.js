import LibraryMusicIcon from '@mui/icons-material/LibraryMusic';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import playlistService from '../../services/playlist';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import PlaylistList from './PlaylistList';

export default function AllPlaylistList() {
  const [playlists, setPlaylists] = useState([]);

  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    playlistService.getSharedAsync, setPlaylists
  );

  return (
    <div className="PageWrapper">
      <PageTitle title="Public playlists" Icon={LibraryMusicIcon} hint="Find the list of all public playlists here." />
      <Filter setIsLoading={setIsLoading} />
      <InfiniteScroll
        dataLength={playlists.length}
        loadMore={nextPage}
        hasMore={!isLoading}
        threshold={150}
      >
        <PlaylistList playlists={playlists} setPlaylists={setPlaylists} />
      </InfiniteScroll>
    </div>
  );
}
