import { Button } from '@mui/material';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import playlistService from '../../services/playlist';
import toastService from '../../services/toast';
import styles from '../../styles/song/DeleteSongModal.module.scss';
import BasicModal from '../BasicModal';
import { Player } from '../ContextProvider';

export default function DeletePlaylistModal({
  playlist, isOpen, onClose, removePlaylist
}) {
  const { playlistId, clearPlayer } = useContext(Player);

  const deletePlaylistAsync = async () => {
    const result = await playlistService.deletePlaylistAsync(playlist.id);

    if (result) {
      onClose();
      removePlaylist();
      if (playlistId === playlist.id) {
        clearPlayer();
      }
      toastService.info(`Playlist '${playlist.name}' has been deleted`);
    }
  };

  return (
    <BasicModal isOpen={isOpen} onClose={onClose}>
      <div className={styles.Wrapper}>
        <div className={styles.Text}>
          Are you sure you want to delete the playlist
          {' '}
          <span className={styles.Highlight}>{playlist.name}</span>
          ?
          <br />
          Once deleted you cannot undo this action.
        </div>
        <div className={styles.Buttons}>
          <Button onClick={onClose}>
            Cancel
          </Button>
          <Button className={styles.DeleteButton} onClick={deletePlaylistAsync}>
            Delete
          </Button>
        </div>
      </div>
    </BasicModal>
  );
}

DeletePlaylistModal.propTypes = {
  playlist: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  removePlaylist: PropTypes.func.isRequired,
};
