import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import { Menu, MenuItem } from '@mui/material';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import playlistService from '../../services/playlist';
import toastService from '../../services/toast';
import styles from '../../styles/playlist/PlaylistActions.module.scss';
import LikeButton from '../LikeButton';
import DeletePlaylistModal from './DeletePlaylistModal';

export default function PlaylistActions({ playlist, updateLikes, removePlaylist }) {
  const history = useHistory();
  const [isLiked, setIsLiked] = useState(playlist.isLiked || false);
  const [isShared, setIsShared] = useState(playlist.isShared || false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const isMenuOpen = Boolean(anchorEl);

  const toggleLikeAsync = async (e) => {
    e.stopPropagation();
    const result = await playlistService.toggleLikeAsync(playlist.id);
    if (result) {
      updateLikes(!isLiked);
      setIsLiked(!isLiked);
    }
  };

  const toggleShareAsync = async (e) => {
    e.stopPropagation();
    const result = await playlistService.toggleShareAsync(playlist.id);
    if (result) {
      setAnchorEl(null);
      setIsShared(!isShared);
      if (isShared) {
        toastService.info(`Playlist '${playlist.name}' is now private`);
      } else {
        toastService.success(`Playlist '${playlist.name}' is now public`);
      }
    }
  };

  const onDeleteClick = () => {
    setAnchorEl(false);
    setIsDeleteModalOpen(true);
  };

  const openPlaylist = () => {
    history.push(`${pageRoutes.PLAYLISTS}/${playlist.id}`);
  };

  return (
    <div className={styles.Actions}>
      <LikeButton isLiked={isLiked} toggleLike={toggleLikeAsync} />
      <MoreHorizIcon onClick={(e) => setAnchorEl(e.target)} className={styles.Icon} />
      <Menu
        id="playlist-actions"
        anchorEl={anchorEl}
        open={isMenuOpen}
        onClose={() => setAnchorEl(null)}
        PaperProps={{
          style: {
            maxHeight: 200,
          },
        }}
      >
        {playlist.isMine && (
        <>
          <MenuItem onClick={toggleShareAsync}>
            {isShared ? 'Make private' : 'Make public'}
          </MenuItem>
          <MenuItem onClick={onDeleteClick}>
            Delete playlist
          </MenuItem>
        </>
        )}
        <MenuItem onClick={openPlaylist}>
          Open playlist
        </MenuItem>
      </Menu>
      {isDeleteModalOpen && (
      <DeletePlaylistModal
        isOpen={isDeleteModalOpen}
        onClose={() => setIsDeleteModalOpen(false)}
        playlist={playlist}
        removePlaylist={removePlaylist}
      />
      )}
    </div>
  );
}

PlaylistActions.propTypes = {
  playlist: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    likeCount: PropTypes.number,
    isLiked: PropTypes.bool,
    isMine: PropTypes.bool,
    isShared: PropTypes.bool,
  }).isRequired,
  updateLikes: PropTypes.func.isRequired,
  removePlaylist: PropTypes.func.isRequired,
};
