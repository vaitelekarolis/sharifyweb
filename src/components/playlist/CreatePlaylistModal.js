import {
  Box, Button, FormControl, TextField
} from '@mui/material';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import playlistService from '../../services/playlist';
import toastService from '../../services/toast';
import BasicModal from '../BasicModal';

export default function CreatePlaylistModal({ isOpen, onClose }) {
  const history = useHistory();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const onSubmitAsync = async (e) => {
    e.preventDefault();
    const result = await playlistService.createAsync({ name, description });
    if (result) {
      history.push(pageRoutes.MY_PLAYLISTS);
      toastService.success(`Created description ${name}`);
      onClose();
    }
  };

  return (
    <BasicModal isOpen={isOpen} onClose={onClose}>
      <div className="ModalTitle">
        Create Playlist
      </div>
      <Box component="form" className="CreateForm" onSubmit={onSubmitAsync}>
        <FormControl>
          <TextField
            required
            label="Name"
            variant="outlined"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormControl>
        <FormControl>
          <TextField
            required
            multiline
            rows={4}
            label="Description"
            variant="outlined"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </FormControl>
        <Button type="submit">
          Create playlist
        </Button>
      </Box>
    </BasicModal>
  );
}

CreatePlaylistModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
