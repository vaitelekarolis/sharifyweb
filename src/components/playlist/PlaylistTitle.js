import PropTypes from 'prop-types';
import { useContext } from 'react';
import styles from '../../styles/playlist/PlaylistTitle.module.scss';
import { Player } from '../ContextProvider';

export default function PlaylistTitle({ id, children }) {
  const { playlistId } = useContext(Player);
  const isCurrentPlaylist = playlistId === id;

  return (
    <span className={isCurrentPlaylist ? styles.Playing : styles.Title}>
      {children}
    </span>
  );
}

PlaylistTitle.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
