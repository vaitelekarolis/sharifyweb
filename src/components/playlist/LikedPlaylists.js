import Favorite from '@mui/icons-material/Favorite';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import playlistService from '../../services/playlist';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import PlaylistList from './PlaylistList';

export default function LikedPlaylists() {
  const [playlists, setPlaylists] = useState([]);

  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    playlistService.getLikedAsync, setPlaylists
  );

  return (
    <div className="PageWrapper">
      <PageTitle
        title="Liked playlists"
        Icon={Favorite}
        hint="The list of all the playlists you liked."
      />
      <Filter setIsLoading={setIsLoading} />
      <InfiniteScroll
        dataLength={playlists.length}
        loadMore={nextPage}
        hasMore={!isLoading}
        threshold={150}
      >
        <PlaylistList title="Liked playlists" playlists={playlists} setPlaylists={setPlaylists} />
      </InfiniteScroll>
    </div>
  );
}
