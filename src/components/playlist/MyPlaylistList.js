import MusicVideoIcon from '@mui/icons-material/MusicVideo';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import playlistService from '../../services/playlist';
import Filter from '../filter/Filter';
import useFilterListener from '../hooks/useFilterListener';
import PageTitle from '../PageTitle';
import PlaylistList from './PlaylistList';

export default function MyPlaylistList() {
  const [playlists, setPlaylists] = useState([]);

  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    playlistService.getMyAsync, setPlaylists
  );

  return (
    <div className="PageWrapper">
      <PageTitle
        title="My Playlists"
        Icon={MusicVideoIcon}
        hint="Sad or happy? Hyped or working out? You can find the playlists you created to vibe here."
      />
      <Filter setIsLoading={setIsLoading} />
      <InfiniteScroll
        dataLength={playlists.length}
        loadMore={nextPage}
        hasMore={!isLoading}
        threshold={150}
      >
        <PlaylistList title="My playlists" playlists={playlists} setPlaylists={setPlaylists} />
      </InfiniteScroll>
    </div>
  );
}
