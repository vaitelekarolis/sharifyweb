import { Button } from '@mui/material';
import { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Link, useHistory, useParams } from 'react-router-dom';
import { pageRoutes } from '../../constants/pageRoutes';
import imageService from '../../services/image';
import playlistService from '../../services/playlist';
import styles from '../../styles/playlist/Playlist.module.scss';
import EditableText from '../EditableText';
import Filter from '../filter/Filter';
import GenresLink from '../GenresLink';
import useFilterListener from '../hooks/useFilterListener';
import SongList from '../song/SongList';
import DeletePlaylistModal from './DeletePlaylistModal';

export default function Playlist() {
  const { id } = useParams();
  const history = useHistory();
  const [playlist, setPlaylist] = useState({});
  const [songs, setSongs] = useState([]);
  const { isLoading, setIsLoading, nextPage } = useFilterListener(
    playlistService.getSongsAsync, setSongs, id
  );
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  const updatePlaylistAsync = async () => {
    const result = await playlistService.getByIdAsync(id);
    setPlaylist(result);
  };

  const updateDescriptionAsync = async (description) => {
    const result = await playlistService.updatePlaylistAsync(playlist.id, description);
    if (result) {
      await updatePlaylistAsync();
    }
  };

  useEffect(updatePlaylistAsync, []);

  return (
    <div className={`${styles.PlaylistWrapper} PageWrapper`}>
      <div className={styles.Wrapper}>
        <div className={styles.Header}>
          <div className={styles.Title}>
            <div className="PageTitle">
              {playlist.name}
            </div>
            <div className={styles.Author}>
              {playlist.createdByPicture && (
              <img
                src={imageService.getImageFromBase64(playlist.createdByPicture)}
                className={styles.CreatedByPicture}
                alt="Creator"
              />
              )}
              <Link to={`${pageRoutes.PROFILE}/${playlist.createdBy}`}>
                {playlist.createdBy}
              </Link>
            </div>
          </div>
          <div className={styles.Actions}>
            <Button className={styles.DeleteButton} onClick={() => setIsDeleteModalOpen(true)}>
              Delete
            </Button>
          </div>
        </div>
        <div>
          <GenresLink genres={playlist.genres} route={pageRoutes.PLAYLISTS} />
        </div>
        <div className={styles.Description}>
          <EditableText
            title="Description"
            isMine={playlist.isMine}
            onChange={updateDescriptionAsync}
            realDescription={playlist.description}
          />
        </div>
      </div>
      <Filter setIsLoading={setIsLoading} />
      {Boolean(songs.length)
        && (
          <>
            <InfiniteScroll
              dataLength={songs.length}
              loadMore={nextPage}
              hasMore={!isLoading}
              threshold={150}
            >
              <SongList
                songs={songs}
                setSongs={setSongs}
                isMine={playlist?.isMine}
                playlistId={playlist?.id}
              />
            </InfiniteScroll>
          </>
        )}
      {isDeleteModalOpen && (
      <DeletePlaylistModal
        playlist={playlist}
        onClose={() => setIsDeleteModalOpen(false)}
        isOpen={isDeleteModalOpen}
        removePlaylist={() => history.push(pageRoutes.MY_PLAYLISTS)}
      />
      )}
    </div>
  );
}
