import PropTypes from 'prop-types';
import { createContext, useEffect, useState } from 'react';
import localStorageService from '../services/localStorage';
import songStorageService from '../services/songStorage';
import userService from '../services/user';

export const UserGeneralInfo = createContext();
export const Player = createContext();

export default function ContextProvider({ children }) {
  const [userGeneralInfo, setUserGeneralInfo] = useState({});
  const [playedSongs, setPlayedSongs] = useState(songStorageService.getPlayedSongs());
  const [previousSongId, setPreviousSongId] = useState(songStorageService.getPreviousSongId());
  const [nextSongId, setNextSongId] = useState(songStorageService.getNextSongId());
  const [playlistId, setPlaylistId] = useState(songStorageService.getPlaylistId());
  const [isPlaying, setIsPlaying] = useState(false);
  const [autoPlay, setAutoPlay] = useState(false);
  const [shuffle, setShuffle] = useState(songStorageService.getShuffle());
  const currentSongId = playedSongs.length ? playedSongs[playedSongs.length - 1] : null;

  const updateUserGeneralInfoAsync = async () => {
    if (localStorageService.isLoggedIn()) {
      const user = await userService.getInfoAsync();
      setUserGeneralInfo(user);
    } else {
      setUserGeneralInfo({});
    }
  };

  const updateStorage = () => {
    songStorageService.setPlayedSongs(playedSongs);
    songStorageService.setNextSongId(nextSongId);
    songStorageService.setPreviousSongId(previousSongId);
    songStorageService.setPlaylistId(playlistId);
    songStorageService.setShuffle(shuffle);
  };

  useEffect(() => {
    updateStorage();
  }, [playedSongs, nextSongId, previousSongId, playlistId, shuffle]);

  const canPlayNext = () => (
    Boolean(nextSongId)
  );

  const canPlayPrevious = () => (
    Boolean(previousSongId)
  );

  const startPlay = (songId, newPlaylistId) => {
    setPlayedSongs([songId]);
    setPlaylistId(newPlaylistId);
  };

  const playNext = () => {
    if (!canPlayNext()) return;
    setPlayedSongs([...playedSongs, nextSongId]);
    setAutoPlay(true);
  };

  const playPrevious = () => {
    if (!canPlayPrevious()) return;
    if (shuffle) {
      const newPlayedSongs = playedSongs;
      newPlayedSongs.pop();
      setPlayedSongs(newPlayedSongs);
    } else {
      setPlayedSongs([previousSongId]);
    }
    setAutoPlay(true);
  };

  const changeShuffle = () => {
    setShuffle(!shuffle);
    setPlayedSongs([currentSongId]);
  };

  const clearPlayer = () => {
    setIsPlaying(false);
    setPlayedSongs([]);
    setNextSongId(null);
    setPlaylistId(null);
  };

  useEffect(async () => {
    await updateUserGeneralInfoAsync();
  }, [localStorageService.isLoggedIn()]);

  return (
    <UserGeneralInfo.Provider
      value={{ userGeneralInfo, setUserGeneralInfo, updateUserGeneralInfoAsync }}
    >
      <Player.Provider value={{
        playedSongs,
        setPlayedSongs,
        nextSongId,
        setNextSongId,
        previousSongId,
        setPreviousSongId,
        shuffle,
        changeShuffle,
        playlistId,
        setPlaylistId,
        currentSongId,
        startPlay,
        playNext,
        playPrevious,
        canPlayNext,
        canPlayPrevious,
        clearPlayer,
        autoPlay,
        setAutoPlay,
        isPlaying,
        setIsPlaying,
      }}
      >
        {children}
      </Player.Provider>
    </UserGeneralInfo.Provider>
  );
}

ContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
