import AddBoxIcon from '@mui/icons-material/AddBox';
import { Button } from '@mui/material';
import PropTypes from 'prop-types';
import styles from '../styles/CreateNewButton.module.scss';

export default function CreateNewButton({ onClick, text }) {
  return (
    <Button
      className={styles.CreateNewButton}
      onClick={onClick}
    >
      <AddBoxIcon />
      {text}
    </Button>
  );
}

CreateNewButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
};

CreateNewButton.defaultProps = {
  text: 'Upload'
};
