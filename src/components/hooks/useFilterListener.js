import qs from 'qs';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

export default function useFilterListener(fetch, set, ...params) {
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);

  const nextPage = () => {
    setIsLoading(true);
    setPage((prevPage) => prevPage + 1);
  };

  useEffect(() => {
    let mounted = true;
    const { search, genre, status } = qs.parse(location.search, { ignoreQueryPrefix: true });
    setPage(1);
    fetch({
      search, genre, status, page: 1
    }, ...params)
      .then((result) => {
        if (mounted) {
          set(result);
        }
        if (result) {
          setIsLoading(false);
        }
      });
    return () => {
      mounted = false;
    };
  }, [location.search]);

  useEffect(() => {
    let mounted = true;
    if (page > 1) {
      const { search, genre, status } = qs.parse(location.search, { ignoreQueryPrefix: true });
      fetch({
        search, genre, status, page
      }, ...params)
        .then((result) => {
          if (mounted) {
            set((prevVal) => [...prevVal, ...result]);
          }
          if (result?.length) {
            setIsLoading(false);
          }
        });
    }
    return () => {
      mounted = false;
    };
  }, [page]);

  return {
    isLoading,
    setIsLoading,
    nextPage,
  };
}
