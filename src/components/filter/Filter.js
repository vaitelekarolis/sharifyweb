/* eslint-disable react/jsx-no-duplicate-props */
import { Search } from '@mui/icons-material';
import {
  Checkbox,
  FormControl,
  InputAdornment,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField
} from '@mui/material';
import _ from 'lodash';
import PropTypes from 'prop-types';
import qs from 'qs';
import { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import songService from '../../services/song';
import styles from '../../styles/filter/Filter.module.scss';

const ITEM_HEIGHT = 60;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export default function Filter({ setIsLoading, hasStatus }) {
  const history = useHistory();
  const location = useLocation();
  const query = qs.parse(location.search, { ignoreQueryPrefix: true });
  const [search, setSearch] = useState(query.search || '');
  const [genre, setGenre] = useState(query.genre ? query.genre.split(',') : []);
  const [genres, setGenres] = useState([]);
  const [status, setStatus] = useState(query.status || '');
  const [statuses, setStatuses] = useState([]);

  const genreId = 'genreId';
  const statusId = 'statusId';

  const onGenreChange = (e) => {
    const {
      target: { value },
    } = e;
    setGenre(
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const onStatusChange = (e) => {
    setStatus(e.target.value);
  };

  useEffect(async () => {
    const result = await songService.getGenresAsync();
    if (result) {
      setGenres(result);
    }
    if (hasStatus) {
      const statusResult = await songService.getStatusesAsync();
      if (statusResult) {
        setStatuses(statusResult);
      }
    }
  }, []);

  useEffect(() => {
    const params = _.omitBy({ search, genre, status }, (val) => val !== 0 && (!val || !val.length));
    const queryParams = new URLSearchParams(params);
    setIsLoading(true);
    history.push({
      pathname: location.pathname,
      search: `?${queryParams.toString()}`
    });
  }, [search, genre, status]);

  useEffect(() => {
    if (query.genre && JSON.stringify(query.genre.split(',')) !== JSON.stringify(genre)) {
      setGenre(query.genre.split(','));
    }
  }, [query.genre]);

  return (
    <div className={styles.FilterWrapper}>
      <FormControl sx={{ width: 250 }}>
        <TextField
          type="search"
          label="Search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            ),
          }}
          inputProps={{
            maxLength: 30,
          }}
        />
      </FormControl>
      <FormControl sx={{ width: 250 }}>
        <InputLabel id={genreId} shrink>Genre</InputLabel>
        <Select
          labelId={genreId}
          multiple
          value={genre}
          onChange={onGenreChange}
          input={<OutlinedInput label="Genre" notched />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
        >
          {genres.map((genreOpt) => (
            <MenuItem key={genreOpt} value={genreOpt}>
              <Checkbox checked={genre.includes(genreOpt)} />
              <ListItemText primary={genreOpt} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {hasStatus && (
      <FormControl sx={{ width: 250 }}>
        <InputLabel id={statusId} shrink>Status</InputLabel>
        <Select
          labelId={statusId}
          value={status}
          onChange={onStatusChange}
          input={<OutlinedInput label="Status" notched />}
          MenuProps={MenuProps}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {statuses.map((statusOpt) => (
            <MenuItem key={statusOpt} value={statusOpt}>
              {statusOpt}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      )}
    </div>
  );
}

Filter.propTypes = {
  setIsLoading: PropTypes.func.isRequired,
  hasStatus: PropTypes.bool,
};

Filter.defaultProps = {
  hasStatus: false,
};
