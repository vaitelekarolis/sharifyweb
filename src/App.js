import { CssBaseline, ThemeProvider } from '@mui/material';
import { initializeApp } from 'firebase/app';
import { useContext } from 'react';
import { Helmet } from 'react-helmet';
import { Route, Switch } from 'react-router-dom';
import { Redirect } from 'react-router-dom/cjs/react-router-dom.min';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import { UserGeneralInfo } from './components/ContextProvider';
import Home from './components/Home';
import Layout from './components/layout/Layout';
import AllPlaylistList from './components/playlist/AllPlaylistList';
import LikedPlaylists from './components/playlist/LikedPlaylists';
import MyPlaylistList from './components/playlist/MyPlaylistList';
import Playlist from './components/playlist/Playlist';
import AllSongs from './components/song/AllSongs';
import LikedSongs from './components/song/LikedSongs';
import SongRequests from './components/song/SongRequests';
import LoginForm from './components/user/LoginForm';
import Profile from './components/user/Profile';
import SignUp from './components/user/SignUpForm';
import { songsStorageUrl } from './constants/constants';
import { pageRoutes } from './constants/pageRoutes';
import localStorageService from './services/localStorage';
import './styles/global.scss';
import { globalTheme } from './styles/globalTheme';

function App() {
  const { userGeneralInfo } = useContext(UserGeneralInfo);

  const firebaseConfig = {
    storageBucket: songsStorageUrl
  };
  initializeApp(firebaseConfig);

  return (
    <div className="App">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Sharify</title>
      </Helmet>
      <ThemeProvider theme={globalTheme}>
        <CssBaseline />
        <Layout>
          <Switch>
            <Route path={pageRoutes.LOGIN} component={LoginForm} />
            <Route path={pageRoutes.SIGN_UP} component={SignUp} />
            <Route path={pageRoutes.SONGS} component={AllSongs} />
            <Route key={pageRoutes.PROFILE} path={`${pageRoutes.PROFILE}/:username`} component={Profile} />
            {localStorageService.isLoggedIn() && userGeneralInfo?.role === 'Admin' && [
              <Route
                key={pageRoutes.SONG_REQUESTS}
                path={pageRoutes.SONG_REQUESTS}
                component={SongRequests}
              />,
            ]}

            {localStorageService.isLoggedIn() && (
              [
                <Route
                  key={pageRoutes.PLAYLISTS}
                  path={`${pageRoutes.PLAYLISTS}/:id`}
                  component={Playlist}
                />,
                <Route
                  key={pageRoutes.PLAYLISTS}
                  exact
                  path={pageRoutes.PLAYLISTS}
                  component={AllPlaylistList}
                />,
                <Route
                  key={pageRoutes.LIKED_SONGS}
                  path={pageRoutes.LIKED_SONGS}
                  component={LikedSongs}
                />,
                <Route
                  key={pageRoutes.LIKED_PLAYLISTS}
                  path={pageRoutes.LIKED_PLAYLISTS}
                  component={LikedPlaylists}
                />,
                <Route
                  key={pageRoutes.MY_PLAYLISTS}
                  path={pageRoutes.MY_PLAYLISTS}
                  component={MyPlaylistList}
                />,
              ]
            )}

            <Route exact path="/home" component={Home} />
            <Redirect exact from="/" to="/home" />
          </Switch>
        </Layout>
      </ThemeProvider>
    </div>
  );
}

export default App;
