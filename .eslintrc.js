module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: [
          'node_modules',
          'enums',
        ],
      },
    },
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'no-plusplus': 0,
    'import/extensions': 0,
    'linebreak-style': 0,
    'import/no-unresolved': 0,
    'react/react-in-jsx-scope': 0,
    'comma-dangle': 'off',
    'max-lines': ['error', { max: 200 }],
  },
};
